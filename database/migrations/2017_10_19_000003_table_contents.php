<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableContents extends Migration
{
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('section_title');
            $table->text('section_description');

            $table->string('title')->nullable()->default("null");
            $table->text('content')->nullable()->default("null");
            
            $table->integer('image_id')->nullable()->unsigned();
            $table->integer('menu_id')->nullable()->unsigned();
            
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
