<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableImages extends Migration
{
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('path')->nullable()->default('/images/default.png');
            $table->string('name')->nullable()->default('default.png');
            $table->string('md5_hash', 32)->nullable()->default();
            
			$table->integer('status')->nullable()->default(0)->unsigned();
			$table->integer('parent_id')->nullable()->unsigned();
			$table->foreign('parent_id')->references('id')->on('images');
        });
    }

    public function down()
    {
        Schema::dropIfExists('images');
    }
}
