<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableServices extends Migration
{
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
			
            $table->string('title');
            $table->text('content');
            $table->string('link');
			
            $table->integer('status')->nullable()->default(1)->unsigned();
			$table->integer('content_id')->nullable()->unsigned();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('services');
    }
}
