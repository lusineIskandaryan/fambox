<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTestimonials extends Migration
{
    public function up()
    {
        Schema::create('testimonials', function (Blueprint $table) {
            $table->increments('id');
			
            $table->string('name');
            $table->text('content');
            $table->string('read_more_url');
            $table->string('position');
			
            $table->integer('is_active')->nullable()->default(0)->unsigned();
            $table->integer('image_id')->nullable()->unsigned();
            $table->integer('status')->nullable()->default(1)->unsigned();
            $table->integer('content_id')->nullable()->unsigned();

            $table->timestamps();
            $table->foreign('image_id')->references('id')->on('images');
        });
    }

    public function down()
    {
        Schema::dropIfExists('testimonials');
    }
}
