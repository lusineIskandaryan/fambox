<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablePortfolios extends Migration
{
    public function up()
    {
        Schema::create('portfolios', function (Blueprint $table) {
            $table->increments('id');
			
            $table->string('title');
            $table->text('content');
            $table->string('link');

            $table->integer('image_id')->nullable()->unsigned();
            $table->integer('status')->nullable()->default(1)->unsigned();
			$table->integer('content_id')->nullable()->unsigned();

            $table->timestamps();
            $table->foreign('image_id')->references('id')->on('images');
        });
    }

    public function down()
    {
        Schema::dropIfExists('portfolios');
    }
}
