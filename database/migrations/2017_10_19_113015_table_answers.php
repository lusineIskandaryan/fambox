<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableAnswers extends Migration
{
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->increments('id');
            $table->text('answer');
            $table->integer('user_id')->nullable()->unsigned();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('messages', function ($table) {
            $table->foreign('answer_id')->references('id')->on('answers');
        });
    }

    public function down()
    {
        Schema::dropIfExists('answers');
    }
}
