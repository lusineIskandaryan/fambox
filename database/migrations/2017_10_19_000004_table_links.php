<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableLinks extends Migration
{
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->increments('id');
            $table->string('facebook')->nullable()->default('0');
            $table->string('twitter')->nullable()->default('0');
            $table->string('google_plus')->nullable()->default('0');
            $table->string('linkedin')->nullable()->default('0');
            $table->string('vkontakte')->nullable()->default('0');
        });
    }

    public function down()
    {
        Schema::dropIfExists('links');
    }
}
