<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTeams extends Migration
{
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');
            
			$table->string('name');
            $table->text('position');
            
			$table->integer('image_id')->nullable()->unsigned();
			$table->integer('link_id')->nullable()->unsigned();
            $table->integer('status')->nullable()->default(1)->unsigned();
			$table->integer('content_id')->nullable()->unsigned();

            $table->timestamps();

            $table->foreign('image_id')->references('id')->on('images');
            $table->foreign('link_id')->references('id')->on('links');
        });
    }

    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
