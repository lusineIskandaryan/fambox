<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableJoiner extends Migration
{
    public function up() {
        Schema::table('portfolios', function (Blueprint $table) {
            $table->foreign('content_id')->references('id')->on('contents');
        });

        Schema::table('teams', function (Blueprint $table) {
            $table->foreign('content_id')->references('id')->on('contents');
        });
		
        Schema::table('services', function (Blueprint $table) {
            $table->foreign('content_id')->references('id')->on('contents');
        });
		
        Schema::table('testimonials', function (Blueprint $table) {
            $table->foreign('content_id')->references('id')->on('contents');
        });
		
		Schema::table('contents', function (Blueprint $table) {
			$table->foreign('image_id')->references('id')->on('images');
			$table->foreign('menu_id')->references('id')->on('menus');
        });
		
		 Schema::table('users', function (Blueprint $table) {
            $table->foreign('image_id')->references('id')->on('images');
        });
    }

    public function down() {
		
    }
}
