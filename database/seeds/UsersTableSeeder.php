<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Model\Other\Image;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $img = new Image;
        $img->save();

        $user = new User;
        $user->name = "FamBox 2017";
        $user->username = "test";
        $user->email = "test@email.com";
        $user->password = bcrypt("123456");
        $user->image_id = $img->id;
        $user->save();
    }
}