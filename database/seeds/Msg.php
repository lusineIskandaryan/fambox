<?php

use Illuminate\Database\Seeder;
use App\Model\Message;
use Faker\Factory as Faker;

class Msg extends Seeder
{
    public function run()
    {
        $faker = Faker::create();
        for($i = 1; $i <= 10; $i++) {
            $msg = new Message;
            $msg->name = $faker->name;
            $msg->subject = $faker->name;
            # Your E-Mail
            $msg->email = "mark.38.98.ii@gmail.com";
            $msg->content = $faker->realText(300);
            $msg->save();
        }
    }
}
