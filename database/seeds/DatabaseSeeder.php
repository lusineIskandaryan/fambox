<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        // $this->call(Msg::class);
        // $this->call(Page::class);
        // $this->call(Portfolios::class);
        // $this->call(Services::class);
        // $this->call(Teams::class);
        // $this->call(Testimonials::class);
        // $this->call(Subscribes::class);
    }
}
