@extends('layouts.maket')
@section('content')
    <div class="col-lg-12">
        <div class="daily-feeds card">
            <div class="card-header">
                <h3 class="h4">Edit Service</h3>
            </div>
            <div class="card-body ">
                <br>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <br>
                <form action="/admin/service/{{ $service->id }}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PUT">

                    <div class="form-group">
                        <label class="form-control-label">Title</label>
                        <input type="text" name="title" value="{{$service->title}}"
                               class="form-control form-control-sm">
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="form-control-label">Link</label>
                        <input type="text" name="link" value="{{$service->link}}"
                               class="form-control form-control-sm">
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="form-control-label">Content</label><br>
                        <textarea id="textarea1" rows="3" type="text" name="content" placeholder="Content.."
                                  class="form-control form-control-sm">{{ $service->content }}</textarea>
                    </div>

                    <br>
                    <div class="form-group">
                        <label class="form-control-label">Content</label><br>
                        <select class="form-control" name="content_id">
                            @foreach($contents as $content)
                                <option value="{{$content->id}}">{{$content->section_title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <br>

                    <div class="input-group">
                        <button type="submit" class="btn btn-primary">seve</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

<script>
$("#content_id").che
</script>