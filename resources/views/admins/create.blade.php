@extends('layouts.maket')

@section('content')
<div class="col-lg-12">
    <div class="daily-feeds card">
        <div class="card-header">
            <h3 class="h4">Creating new admin</h3>
        </div>
        <div class="card-body ">
            <br>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <br>
            <form action="/admin/administrator" method="POST">
                <div class="row">
                    <div class="col-md-3">
                    <img id="img-avatar" class="rounded-circle" src="/images/default.png" width="100%" alt="default"><br><br>
                    <label class="btn btn-default btn-file" style="width: 100%;">
                        Avatar: Select image...<input type="file" accept="image/*,image/jpeg" style="display: none;" name="image">
                    </label>
                        <a id="clear-btn" href="#" class="btn btn-danger" style="width: 100%;">Remove this image</a>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group-material">
                            <input id="name" type="text" name="name" required class="input-material">
                            <label for="name" class="label-material">Name</label>
                        </div>
                        <div class="form-group-material">
                            <input id="username" type="text" name="username" required class="input-material">
                            <label for="username" class="label-material">User Name</label>
                        </div>
                        <div class="form-group-material">
                            <input id="email" type="email" name="email" required class="input-material">
                            <label for="email" class="label-material">Email Address</label>
                        </div>
                        <div class="form-group-material">
                            <input id="password" type="password" name="password" required class="input-material">
                            <label for="password" class="label-material">Password</label>
                        </div>
                        <div class="line"> </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <a href="/admin/administrator/" class="btn btn-secondary">Cancel</a>
                                <button type="submit" class="btn btn-primary">Registration</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection