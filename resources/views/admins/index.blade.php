@extends('layouts.maket')

@section('content')
<div class="col-lg-12">
    <div class="daily-feeds card">
        <div class="card-header">
            <h3 class="h4">Admins Manager</h3>
        </div>
        <div class="card-body">
            @if(is_string($inf))
                <div class="alert alert-success" role="alert">{{ $inf }}</div>
            @endif
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th width="100%">Admin Name</th>
                        <th>UserName</th>
                        <th>@Mail</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $value)
                    <tr>
                        <th scope="row">{{ $value->id }}</th>
                        <td><img class="avatar" src="/images/{{ $value->image->name }}" alt="err"> {{ $value->name }}</td>
                        <td>{{ $value->username }}</td>
                        <td>{{ $value->email }}</td>
                        <td>
                            <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                <div class="btn-group" role="group">
                                    <a href="/admin/administrator/{{ $value->id }}" class="btn btn-xs btn-success"> Show </a>
                                </div>
                                <div class="btn-group" role="group">
                                    <a href="/admin/administrator/{{ $value->id }}/edit" class="btn btn-xs btn-success"> Edit </a>
                                </div>
                                <div class="btn-group" role="group">
                                    <form action="/admin/administrator/{{ $value->id }}" method="POST">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="submit" class="btn btn-xs btn-danger" onclick="return confirm('Confirm delete?')" value="Delete">
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $users->appends(['search' => Request::get('search')])->render() !!} 
        </div>
    </div>
</div>
@endsection