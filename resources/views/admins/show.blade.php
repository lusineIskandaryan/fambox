@extends('layouts.maket')

@section('content')
<div class="col-lg-12">
    <div class="daily-feeds card">
        <div class="card-header">
            <h3 class="h4">Admin profile</h3>
        </div>
        <div class="card-body ">
        <br>
        <div class="row">
            <div class="col-md-2">
                <img class="rounded-circle" src="/images/{{ $user->image->name }}" width="100%" alt="default"><br><br>
                <a href="/admin/administrator/{{ $user->id }}/edit" class="btn btn-default" style="width: 100%;">Edit account</a><br><br>
                <form action="/admin/administrator/{{ $user->id }}" method="POST">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" style="width: 100%;" class="btn btn-xs btn-danger" onclick="return confirm('Confirm delete?')" value="Remove account">
                </form>
            </div>
            <div class="col-md-10">
                <ul class="list-group">
                    <li class="list-group-item">
                        <b>Name: </b> {{$user->name}}
                    </li>
                    <li class="list-group-item">
                        <b>UserName: </b> {{$user->username}}
                    </li>
                    <li class="list-group-item">
                        <b>Email: </b> {{$user->email}}
                    </li>
                </ul>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection