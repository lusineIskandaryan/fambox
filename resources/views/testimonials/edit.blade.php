@extends('layouts.maket')

@section('content')
<div class="col-lg-12">
    <div class="daily-feeds card">
        <div class="card-header">
            <h3 class="h4">Editing testimonial</h3>
        </div>
        <div class="card-body ">
            <br>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <br>
            <form enctype="multipart/form-data" action="/admin/testimonial/{{ $testimonials->id }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <div class="row">
                    <div class="col-md-3">
                        <img id="img-avatar" class="rounded-circle" src="/images/{{ $testimonials->image->name }}" width="100%" alt="default"><br><br>
                        <label class="btn btn-default btn-file" style="width: 100%;">
                            Avatar: Select image...<input type="file" accept="image/*,image/jpeg" style="display: none;" name="image">
                        </label>
                        <a id="clear-btn" href="#" class="btn btn-danger" style="width: 100%;">Remove this image</a>
                    </div>
                    <div class="col-md-9">

                        <div class="form-group">
                                <label class="form-control-label">Name</label>
                            <input type="text" name="name" placeholder="Name" class="form-control form-control-sm" value="{{ $testimonials->name }}">
                        </div>

                        <div class="form-group">
                                <label class="form-control-label">Position</label>
                            <input type="text" name="position" placeholder="Position" class="form-control form-control-sm" value="{{ $testimonials->position }}">
                        </div>

                        <div class="form-group">
                            <label class="form-control-label">Url</label>
                            <input type="text" name="read_more_url" placeholder="Url" class="form-control form-control-sm" value="{{ $testimonials->read_more_url }}">
                        </div>

                        <br>
                        <div class="form-group">
                            <label class="form-control-label">Content</label><br>
                            <select class="form-control" name="content_id">
                                @foreach($contents as $content)
                                    <option value="{{$content->id}}">{{$content->section_title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <br>

                        <div class="i-checks">
                            <label>Active on the website?</label>
                        </div>
                        <div class="i-checks">
                            <input id="active1" name="is_active" type="radio" value="1" name="a" class="radio-template">
                            <label for="active1">Active</label>
                        </div>
                        <div class="i-checks">
                            <input id="active0" name="is_active" type="radio" value="0" name="a" class="radio-template">
                            <label for="active0">Inactive</label>
                        </div>
                        
                        <br>
                        <div class="form-group">
                            <label class="form-control-label">Testimonial</label><br>
                            <textarea id="textarea1" type="text" rows="12" placeholder="Testimonial" class="form-control" name="content">{{ $testimonials->content }}</textarea>
                        </div>

                        <div class="line"> </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <a href="/admin/testimonials" class="btn btn-secondary">Cancel</a>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $("#active{{ $testimonials->is_active }}").attr('checked', '');
</script>
<script src="/js/admin.js"></script>
@endsection