@extends('layouts.maket')

@section('content')
<div class="col-lg-12">
    <div class="daily-feeds card">
        <div class="card-header">
            <h3 class="h4">Testimonial Manager</h3>
        </div>
        <div class="card-body">
            @if(!empty(Session::get('flash_message')))
                <div class="alert alert-success" role="alert">{{ Session::get('flash_message')}} </div>
            @endif
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Position</th>
                        <th>URL</th>
                        <th width="100%">Testimonial</th>
                        <th>IsActive</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($testimonials as $value)
                    <tr>
                        <th scope="row">{{ $value->id }}</th>
                        <td style="display: flex;"><img class="avatar" src="/images/{{ $value->image->name }}" alt="err"> {{ $value->name }}</td>
                        <td>{{ $value->read_more_url }}</td>
                        <td>{{ $value->position }}</td>
                        <td class="text">{{ $value->content }}</td>
                        <td>
                            @if($value->is_active != "0")
                            <div class="btn btn-xs btn-success" style="width:70px;">Active</div>
                            @else
                            <div class="btn btn-xs btn-danger" style="width:70px;">Inactive</div>
                            @endif
                        </td>
                        <td>
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group" role="group">
                                    <a href="/admin/testimonial/{{ $value->id }}/edit" class="btn btn-xs btn-success"> Edit </a>
                                </div>
                                <div class="btn-group" role="group">
                                    <form action="/admin/testimonial/{{ $value->id }}" method="POST">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="submit" class="btn btn-xs btn-danger" 
                                        onclick="return confirm('Confirm delete?')" value="Delete">
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $testimonials->appends(['search' => Request::get('search')])->render() !!} 
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection