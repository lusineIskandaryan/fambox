@extends('layouts.maket')

@section('content')
<div class="col-lg-12">
    <div class="daily-feeds card">
        <div class="card-header">
            <h3 class="h4">Portfolio Manager</h3>
        </div>
        <div class="card-body">
            @if(!empty(Session::get('flash_message')))
                <div class="alert alert-success" role="alert">{{ Session::get('flash_message')}} </div>
            @endif
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th width="100%">Link</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($portfolios as $portfolio)
                    <tr>
                        <th scope="row">{{ $portfolio->id }}</th>
                        <td>{{ $portfolio->title }}</td>
                        <td>
                            @if($portfolio->status != "0")
                            <div class="btn btn-xs btn-success" style="width:70px;margin: 0">Active</div>
                            @else
                            <div class="btn btn-xs btn-danger" style="width:70px;margin: 0">Inactive</div>
                            @endif
                        </td>
                        <td><a href="{{ $portfolio->link }}">{{ $portfolio->link }}</a></td>
                        <td>
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group" role="group">
                                    <a href="/admin/portfolio/{{ $portfolio->id }}" class="btn btn-xs btn-success"> Show </a>
                                </div>
                                <div class="btn-group" role="group">
                                    <a href="/admin/portfolio/{{ $portfolio->id }}/edit" class="btn btn-xs btn-success"> Edit </a>
                                </div>
                                <div class="btn-group" role="group">
                                    <form action="/admin/portfolio/{{ $portfolio->id }}" method="POST">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="submit" class="btn btn-xs btn-danger" 
                                        onclick="return confirm('Confirm delete?')" value="Delete">
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $portfolios->appends(['search' => Request::get('search')])->render() !!}
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection