@extends('layouts.maket')
@section('content')
<div class="col-lg-12">
    <div class="daily-feeds card">
        <div class="card-header">
            <h3 class="h4">Add new portfolio</h3>
        </div>
        <div class="card-body ">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form enctype="multipart/form-data" action="/admin/portfolio" method="POST">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-control-label">Title</label>
                            <input type="text" name="title" placeholder="Title" class="form-control form-control-sm">
                        </div>
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon" style="width: 40px;"><i class="fa fa-link"></i></span>
                            <input type="text" placeholder="Link" class="form-control" name="link">
                        </div>
                        <br>
                        <div class="form-group">
                            <label class="form-control-label">Small description..</label>
                            <textarea id="textarea1" rows="3" type="text" name="content" placeholder="Content.."
                            class="form-control form-control-sm"></textarea>
                        </div>
                        <br>
                        <div class="form-group">
                            <label class="form-control-label">Content</label><br>
                            <select class="form-control" name="content_id">
                                @foreach($contents as $content)
                                    <option value="{{$content->id}}">{{$content->section_title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <br>
                        <div class="form-group">
                            <img id="img-avatar" class="rounded-circle" src="/images/default.png" width="20%" alt="default"><br><br>
                            <label class="btn btn-default btn-file" style="width: 100%;">
                                Select image...<input type="file" accept="image/*,image/jpeg" style="display: none;" name="image">
                            </label>
                            <a id="clear-btn" href="#" class="btn btn-danger" style="width: 100%;">Remove this image</a>
                        </div>
                        <div class="line"> </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <a href="/admin/portfolio/" class="btn btn-secondary">Cancel</a>
                                <button type="submit" class="btn btn-primary">Add Portfolio</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
