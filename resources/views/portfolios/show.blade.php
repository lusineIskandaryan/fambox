@extends('layouts.maket')

@section('content')
<div class="col-lg-12">
    <div class="daily-feeds card">
        <div class="card-header">
            <h3 class="h4">{{$portfolio->title}}</h3>
        </div>
        <div class="card-body ">
        <br>
        <div class="row">
            <div class="col-md-2">
                <img class="rounded-circle" src="/images/{{ $portfolio->image->name }}" width="100%" alt="default"><br><br>
                <a href="/admin/portfolio/{{ $portfolio->id }}/edit" class="btn btn-default" style="width: 100%;">Edit</a><br><br>
                <form action="/admin/portfolio/{{ $portfolio->id }}" method="POST">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" style="width: 100%;" class="btn btn-xs btn-danger" 
                        onclick="return confirm('Confirm delete?')" value="Remove member">
                </form>
            </div>
            <div class="col-md-10">
                <ul class="list-group">
                    <li class="list-group-item">
                        <?php echo $portfolio->content;?>
                    </li>
                    <li class="list-group-item">
                        <b>Link: </b> {{$portfolio->link}}
                    </li>
                    <li class="list-group-item">
                        <b>Date: </b> {{$portfolio->updated_at}}
                    </li>
                </ul>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection