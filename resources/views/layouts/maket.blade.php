<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FamBox</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <link rel="shortcut icon" href="/favicons.ico" />

    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css" id="theme-stylesheet">
    <script src="https://use.fontawesome.com/99347ac47f.js"></script>
    <link rel="stylesheet" href="https://file.myfontastic.com/da58YPMQ7U5HY8Rb6UxkNf/icons.css">

    <script>
        window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]); ?>;
    </script>
    <link rel="stylesheet" href="/text_editor/mark.min.css">

</head>
<body>
<div class="page home-page">
    <!-- Main Navbar-->
    <header class="header" style="margin-bottom: 45px;">
        <nav class="navbar navbar-expand-md navbar-dark fixed-top">
            <!-- Search Box-->
            <div class="search-box">
                <button class="dismiss"><i class="icon-close"></i></button>
                <form id="searchForm" action="#" role="search">
                    <input type="search" placeholder="What are you looking for..." class="form-control">
                </form>
            </div>
            <div class="container-fluid">
                <div class="navbar-holder d-flex align-items-center justify-content-between">
                    <div class="navbar-header">
                        <a href="/" class="navbar-brand">
                            <div class="brand-text brand-big hidden-lg-down"><span>FamBox </span></div>
                            <div class="brand-text brand-small"><img src="/images/default.png" style="width: 68px;margin: -10px -7px -10px -20px;"></div>
                        </a>
                        <a id="toggle-btn" href="#" class="menu-btn active">
                            <span></span><span></span><span></span>
                        </a>
                    </div>
                
                    <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                        <li class="nav-item dropdown">
                            <a href="/admin/msg/notverified" class="nav-link">
                                <i class="fa fa-bell-o"></i>
                                @if(($count = sizeof(App\Model\Message::whereNull('answer_id')->get())))
                                    <span class="badge bg-red" id="msgcount">{{ $count }}</span>
                                @endif
                            </a>
                        </li>
                        @guest
                        <a class="nav-link logout" href="{{ route('login') }}">Login</a>
                        @else
                            <a href="{{ route('logout') }}" class="nav-link logout" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    
    <div class="page-content d-flex align-items-stretch">
        <nav class="side-navbar">
            <div class="sidebar-header d-flex align-items-center">
                <div class="bg2"></div>
                <div class="avatar">
                    <img src="/images/{{ Auth::user()->image->name }}" class="img-fluid rounded-circle" style="cursor: default;width: 77px;height: 55px;">
                </div>
                <div class="title">
                    <h1 class="h4"><a href="/admin/administrator/{{ Auth::user()->id }}">{{Auth::user()->name}}</a></h1>
                    <p>{{Auth::user()->email}}</p>
                </div>
            </div>

            <span class="heading">Main menu</span>
            <ul class="list-unstyled " id="storage-dropdown">
                <li><a href="#administrator" aria-expanded="false" data-toggle="collapse" id="administrator1">
                    <i class="icon-user"></i>Admin manager </a>
                    <ul id="administrator" class="collapse list-unstyled">
                        <li><a href="/admin/administrator">Admins</a></li>
                        <li><a href="/admin/administrator/create">Create admin</a></li>
                    </ul>
                </li>
                
                <li><a href="#msg" aria-expanded="false" data-toggle="collapse" id="msg1">
                    <i class="icon-mail"></i>Messages </a>
                    <ul id="msg" class="collapse list-unstyled">
                        <li><a href="/admin/msg/notverified">Unanswered</a></li>
                        <li><a href="/admin/msg/verified">Answering</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#c" aria-expanded="false" data-toggle="collapse" id="c1">
                        <i class="fa fa-file-text-o"></i>Content manager </a>
                    <ul id="c" class="collapse list-unstyled">
                        <li><a href="/admin/content">Contents</a></li>
                        <li><a href="/admin/content/create">Add Contents</a></li>
                    </ul>
                </li>

                <li><a href="#team" aria-expanded="false" data-toggle="collapse" id="team1">
                        <i class="icon-user"></i>Team manager </a>
                    <ul id="team" class="collapse list-unstyled">
                        <li><a href="/admin/team">Members</a></li>
                        <li><a href="/admin/team/create">Add member</a></li>
                    </ul>
                </li>

                <li><a href="#services" aria-expanded="false" data-toggle="collapse" id="service1">
                        <i class="fa fa-cogs"></i>Service manager </a>
                    <ul id="services" class="collapse list-unstyled">
                        <li><a href="/admin/service">Services</a></li>
                        <li><a href="/admin/service/create">Add service</a></li>
                    </ul>
                </li>
                
                <li><a href="#testimonial" aria-expanded="false" data-toggle="collapse" id="testimonial1">
                    <i class="fa fa-commenting"></i>Testimonial manager </a>
                    <ul id="testimonial" class="collapse list-unstyled">
                        <li><a href="/admin/testimonial">Testimonials</a></li>
                        <li><a href="/admin/testimonial/create">Add testimonial</a></li>
                    </ul>
                </li>

                <li><a href="#portfolio" aria-expanded="false" data-toggle="collapse" id="portfolio1">
                    <i class="fa fa-briefcase"></i>Portfolio manager </a>
                    <ul id="portfolio" class="collapse list-unstyled">
                        <li><a href="/admin/portfolio">Portfolios</a></li>
                        <li><a href="/admin/portfolio/create">Add portfolio</a></li>
                    </ul>
                </li>
                
                <li>
                    <a href="#mt" aria-expanded="false" data-toggle="collapse" id="mt1">
                        <i class="fa fa-at"></i>Mail template </a>
                    <ul id="mt" class="collapse list-unstyled">
                        <li><a href="/admin/template">Mail template</a></li>
                        <li><a href="/admin/template/create">Add template</a></li>
                    </ul>
                </li>
                
                <li>
                    <a href="/admin/subscribe"> 
                        <i class="fa fa-bell"></i>Subscribe manager
                    </a>
                </li>

                <li>
                    <a href="#menu" aria-expanded="false" data-toggle="collapse" id="menu1">
                        <i class="fa fa-list-ul"></i>Menue manager </a>
                    <ul id="menu" class="collapse list-unstyled">
                        <li><a href="/admin/menue">Menue</a></li>
                        <li><a href="/admin/menue/create">Add Menue</a></li>
                    </ul>
                </li>
            </ul>

        </nav>
      
        <div class="content-inner">
            <header class="page-header">
                <div class="container-fluid">
                    <h2 class="no-margin-bottom">@yield('page-header')</h2>
                </div>
            </header>
            <br>
            @yield('content')
            <br>
            <footer class="main-footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <p>Copyright &copy; 2017-2027</p>
                        </div>
                        <div class="col-sm-6 text-right">
                            <p>Разработано знатоками своего дела Designed by <a href="https://fambox.x" class="external">FamBox</a>
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>

<!-- Javascript files-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="/js/tether.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.cookie.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script src="/js/front.js"></script>
@yield('script')
<script src="/js/admin.js"></script>
<script src="/text_editor/jquery.mark.js"></script>
<script>
    $(document).ready(function() {
        $('#textarea1').richText();
        $('#textarea2').richText();
        $('#textarea3').richText();
    });
</script>
</body>
</html>