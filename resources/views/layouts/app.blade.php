<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FamBox</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <link rel="shortcut icon" href="/favicons.ico" />

    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css" id="theme-stylesheet">
    <script src="https://use.fontawesome.com/99347ac47f.js"></script>
    <link rel="stylesheet" href="https://file.myfontastic.com/da58YPMQ7U5HY8Rb6UxkNf/icons.css">
    
    <script>
        window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]); ?>;
    </script>
</head>
<body>
<!-- Main Navbar-->
<header class="header" style="margin-bottom: 45px;">
    <nav class="navbar navbar-expand-md navbar-dark fixed-top">
        <!-- Search Box-->
        <div class="search-box">
            <button class="dismiss"><i class="icon-close"></i></button>
            <form id="searchForm" action="#" role="search">
                <input type="search" placeholder="What are you looking for..." class="form-control">
            </form>
        </div>
        <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
                <div class="navbar-header">
                    <a href="/" class="navbar-brand">
                        <div class="brand-text brand-big hidden-lg-down"><span>FamBox </span></div>
                        <div class="brand-text brand-small"><img src="/img/fb.png" style="width: 29px;"></div>
                    </a>
                </div>
            
                <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                    @guest
                    <a class="nav-link logout" href="{{ route('login') }}">Login</a>
                    @else
                        <a href="{{ route('logout') }}" class="nav-link logout" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
</header>
@yield('content')

<footer class="main-footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <p>Copyright &copy; 2017-201*</p>
            </div>
            <div class="col-sm-6 text-right">
                <p>Разработано знатоками своего дела Designed by <a href="https://fambox.x" class="external">FamBox</a>
                </p>
            </div>
        </div>
    </div>
</footer>

<!-- Javascript files-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="/js/tether.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.cookie.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script src="/js/front.js"></script>
@yield('script')
<script src="/js/admin.js"></script>
</body>
</html>