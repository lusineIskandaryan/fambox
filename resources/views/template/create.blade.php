@extends('layouts.maket')
@section('content')
    <div class="col-lg-12">
        <div class="daily-feeds card">
            <div class="card-header">
                <h3 class="h4">Create sesvices</h3>
            </div>
            <div class="card-body ">
                <br>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <br>
                    <form action="/admin/template" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                        <label class="form-control-label">Name</label>
                            <input type="text" name="name"
                                   class="form-control form-control-sm" placeholder='Name'>
                        </div>

                        <div class="input-group">
                        <label class="form-control-label">Content</label>
                        </div>
                        <br>
                        <div>
                            <textarea id="textarea1" name="content" rows="3" style="width: 100%" placeholder="Content"></textarea>
                        </div>
                        <br>
                        <div class="input-group">
                            <button type="submit" class="btn btn-primary">Add</button>
                        </div>
                    </form>
            </div>
        </div>
@endsection