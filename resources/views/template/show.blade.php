@extends('layouts.maket')

@section('content')
<div class="col-lg-12">
    <div class="daily-feeds card">
        <div class="card-header">
            <h3 class="h4">{{$template->name}}</h3>
        </div>
        <div class="card-body ">
        <br>
        <div class="row">
            <div class="col-md-12">
                <iframe src="/admin/template/{{$template->id}}/preview" frameborder="0" width="100%" height="800"></iframe>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection