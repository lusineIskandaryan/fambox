@extends('layouts.maket')
@section('content')
    <div class="col-lg-12">
        <div class="daily-feeds card">
            <div class="card-header">
                <h3 class="h4">{{$title}}</h3>
            </div>
            <div class="card-body ">
                <br>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <br>
                <form action="/admin/template/{{ $template->id }}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">
                    <label class="form-control-label">Name</label>
                    <input type="text" name="name" value="{{$template['name']}}"
                               class="form-control form-control-sm">
                    </div>

                    <div class="input-group">
                        <label class="form-control-label">Text</label>
                    </div>
                    <br>
                    <div>
                        <textarea id="textarea1" name="content" rows="3" style="width: 100%"> {!!  $template['content']!!} </textarea>
                    </div>
                    <br>
                    <div class="input-group">
                        <button type="submit" class="btn btn-primary">seve</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection