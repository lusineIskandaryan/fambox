@extends('layouts.maket')
@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Services</h3>
            </div>
            <div class="card-body">
                @if($template)
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th width="100%">Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($template as $_template)
                            <tr>
                                <td>{{$_template->id}}</td>
                                <td>{{$_template->name}}</td>
                                <td>
                                    <div class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group btn-group-justified" role="group">
                                            <div class="btn-group" role="group">
                                                <a href="/admin/template/{{ $_template->id }}" class="btn btn-xs btn-success"> Preview </a>
                                            </div>
                                        </div>
                                        <div class="btn-group" role="group">
                                            <a href="/admin/template/{{ $_template->id }}/edit" class="btn btn-xs btn-success">
                                            Edit </a>
                                         </div>
                                        <div class="btn-group" role="group">
                                            <form action="/admin/template/{{ $_template->id }}" method="POST">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <input type="submit" class="btn btn-xs btn-danger"
                                                    onclick="return confirm('Confirm delete?')" value="Delete">
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('script')
@endsection