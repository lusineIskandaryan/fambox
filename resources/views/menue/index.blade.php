@extends('layouts.maket')
@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Services</h3>
            </div>
            <div class="card-body">
                @if(!empty(Session::get('flash_message')))
                    <div class="alert alert-success" role="alert">{{ Session::get('flash_message')}} </div>
                @endif
                @if($menue)
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th width="30%">Name</th>
                            <th width="30%">Uri</th>
                            <th width="30%">Ordering</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($menue as $menu)
                            <tr>
                                <td>{{$menu->id}}</td>
                                <td>{{$menu->name}}</td>
                                <td>{{ $menu->uri }}</td>
                                <td>{{$menu->ordering}}</td>

                                <td>
                                    <div class="btn-group btn-group-justified" role="group">
                                        <div class="btn-group" role="group">
                                            <a href="/admin/menue/{{ $menu->id }}/edit" class="btn btn-xs btn-success">
                                            Edit </a>
                                         </div>
                                        <div class="btn-group" role="group">
                                            <form action="/admin/menue/{{ $menu->id }}" method="POST">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <input type="submit" class="btn btn-xs btn-danger"
                                                    onclick="return confirm('Confirm delete?')" value="Delete">
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('script')
@endsection