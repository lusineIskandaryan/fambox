@extends('layouts.maket')
@section('content')
    <div class="col-lg-12">
        <div class="daily-feeds card">
            <div class="card-header">
                <h3 class="h4">Create sesvices</h3>
            </div>
            <div class="card-body ">
                <br>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <br>


                <form action="/admin/menue" method="POST" enctype="multipart/form-data">

                    <div class="form-group">
                        <label class="form-control-label">Name</label>
                        <input type="text" name="name"
                               class="form-control form-control-sm">
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="form-control-label">Uri</label>
                        <input type="text" name="uri"
                               class="form-control form-control-sm">
                    </div>
                    <br>

                    <div class="form-group">
                        <label class="form-control-label">Ordering</label>
                        <input type="text" name="ordering"
                               class="form-control form-control-sm">
                    </div>

                    <br>
                    <div class="form-group">
                        <label class="form-control-label">Content</label><br>
                        <select class="form-control" name="parent_id" id="">
                            <option value="0">Is Parent</option>
                            @foreach($menue as $_menu)
                                <option value="{{$_menu->id}}">[{{$_menu->ordering}}] - {{$_menu->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <br>

                    <div class="input-group">
                        <button type="submit" class="btn btn-primary">seve</button>
                    </div>
                </form>



            </div>
        </div>
@endsection