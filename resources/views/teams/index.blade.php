@extends('layouts.maket')

@section('content')
    <div class="col-lg-12">
        <div class="daily-feeds card">
            <div class="card-header">
                <h3 class="h4">Team Manager</h3>
            </div>
            <div class="card-body">
                @if(!empty(Session::get('flash_message')))
                    <div class="alert alert-success" role="alert">{{ Session::get('flash_message')}} </div>
                @endif
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th width="100%">Name</th>
                        <th>Position</th>
                        <th>URL</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($teams as $value)
                        <tr>
                            <th scope="row">{{ $value->id }}</th>
                            <td><img class="avatar" src="/images/{{ $value->image->name }}" alt="err"> {{ $value->name }}
                            </td>
                            <td>{{ $value->position }}</td>
                            <td>
                                @if($value->links)
                                <div class="btn-group btn-group-justified" role="group">
                                    @if($value->links->facebook != "0")
                                        <div class="btn-group" role="group">
                                            <a href="{{ $value->links->facebook }}" class="btn btn-xs btn-default"><i
                                                        class="fa fa-facebook"></i></a>
                                        </div>
                                    @endif

                                    @if($value->links->twitter != "0")
                                        <div class="btn-group" role="group">
                                            <a href="{{ $value->links->twitter }}" class="btn btn-xs btn-default"><i
                                                        class="fa fa-twitter"></i></a>
                                        </div>
                                    @endif

                                    @if($value->links->google_plus != "0")
                                        <div class="btn-group" role="group">
                                            <a href="{{ $value->links->google_plus }}" class="btn btn-xs btn-default"><i
                                                        class="fa fa-google-plus"></i></a>
                                        </div>
                                    @endif

                                    @if($value->links->linkedin != "0")
                                        <div class="btn-group" role="group">
                                            <a href="{{ $value->links->linkedin }}" class="btn btn-xs btn-default"><i
                                                        class="fa fa-linkedin"></i></a>
                                        </div>
                                    @endif
                                    @if($value->links->vkontakte != "0")
                                        <div class="btn-group" role="group">
                                            <a href="{{ $value->links->vkontakte }}" class="btn btn-xs btn-default"><i
                                                        class="fa fa-vk"></i></a>
                                        </div>
                                    @endif
                                </div>
                                @endif
                            </td>
                            <td>
                                <div class="btn-group btn-group-justified" role="group">
                                    <div class="btn-group" role="group">
                                        <a href="/admin/team/{{ $value->id }}" class="btn btn-xs btn-success"> Show </a>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <a href="/admin/team/{{ $value->id }}/edit" class="btn btn-xs btn-success">
                                            Edit </a>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <form action="/admin/team/{{ $value->id }}" method="POST">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="submit" class="btn btn-xs btn-danger"
                                                   onclick="return confirm('Confirm delete?')" value="Delete">
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $teams->appends(['search' => Request::get('search')])->render() !!}
            </div>
        </div>
    </div>
@endsection

@section('script')
@endsection