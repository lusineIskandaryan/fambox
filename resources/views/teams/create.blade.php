@extends('layouts.maket')

@section('content')
<div class="col-lg-12">
    <div class="daily-feeds card">
        <div class="card-header">
            <h3 class="h4">Add new member</h3>
        </div>
        <div class="card-body ">
            <br>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <br>
            <form enctype="multipart/form-data" action="/admin/team" method="POST">
                <div class="row">
                    <div class="col-md-3">
                        <img id="img-avatar" class="rounded-circle" src="/images/default.png" width="100%" alt="default"><br><br>
                        <label class="btn btn-default btn-file" style="width: 100%;">
                            Avatar: Select image...<input type="file" accept="image/*,image/jpeg" style="display: none;" name="image">
                        </label>
                        <a id="clear-btn" href="#" class="btn btn-danger" style="width: 100%;">Remove this image</a>
                    </div>
                    <div class="col-md-9">

                        <div class="form-group">
                            <label class="form-control-label">Member Name</label>
                            <input type="text" name="name" placeholder="Member Name" class="form-control form-control-sm">
                        </div>

                        <div class="form-group">
                            <label class="form-control-label">Position</label>
                            <input type="text" name="position" placeholder="Position" class="form-control form-control-sm">
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 40px;"><i class="fa fa-facebook"></i></span>
                                <input type="text" placeholder="FaceBook link" class="form-control" name="fb">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 40px;"><i class="fa fa-twitter"></i></span>
                                <input type="text" placeholder="Twitter link" class="form-control" name="tw">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 40px;"><i class="fa fa-google-plus"></i></span>
                                <input type="text" placeholder="Google+ link" class="form-control" name="g">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 40px;"><i class="fa fa-linkedin"></i></span>
                                <input type="text" placeholder="In link" class="form-control" name="in">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" style="width: 40px;"><i class="fa fa-vk"></i></span>
                                <input type="text" placeholder="VK link" class="form-control" name="vk">
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <label class="form-control-label">Page</label><br>
                            <select class="form-control" name="content_id">
                                @foreach($contents as $content)
                                    <option value="{{$content->id}}">{{$content->section_title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="line"> </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <a href="/admin/team/" class="btn btn-secondary">Cancel</a>
                                <button type="submit" class="btn btn-primary">Add member</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection