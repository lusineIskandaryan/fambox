@extends('layouts.maket')

@section('content')
<div class="col-lg-12">
    <div class="daily-feeds card">
        <div class="card-header">
            <h3 class="h4">Member profile</h3>
        </div>
        <div class="card-body ">
        <br>
        <div class="row">
            <div class="col-md-2">
                <img class="rounded-circle" src="/images/{{ $team->image->name }}" width="100%" alt="default"><br><br>
                <a href="/admin/team/{{ $team->id }}/edit" class="btn btn-default" style="width: 100%;">Edit</a><br><br>
                <form action="/admin/team/{{ $team->id }}" method="POST">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" style="width: 100%;" class="btn btn-xs btn-danger" 
                        onclick="return confirm('Confirm delete?')" value="Remove member">
                </form>
            </div>
            <div class="col-md-10">
                <ul class="list-group">
                    <li class="list-group-item">
                        <b>Name: </b> {{$team->name}}
                    </li>
                    <li class="list-group-item">
                        <b>Position: </b> {{$team->position}}
                    </li>
                    @if($team->link_id)
                        @if($team->links->facebook != "0")
                        <li class="list-group-item">
                            <b>FaceBook: </b> 
                            <a href="{{ $team->links->facebook }}">{{ $team->links->facebook }}</a>
                        </li>
                        @endif

                        @if($team->links->twitter != "0")
                        <li class="list-group-item">
                            <b>Twitter: </b> 
                            <a href="{{ $team->links->twitter }}">{{ $team->links->twitter }}</a>
                        </li>
                        @endif
                        
                        @if($team->links->google_plus != "0")
                        <li class="list-group-item">
                            <b>Google+: </b> 
                            <a href="{{ $team->links->google_plus }}">{{ $team->links->google_plus }}</a>
                        </li>
                        @endif

                        @if($team->links->linkedin != "0")
                        <li class="list-group-item">
                            <b>IN: </b> 
                            <a href="{{ $team->links->linkedin }}">{{ $team->links->linkedin }}</a>
                        </li>
                        @endif

                        @if($team->links->vkontakte != "0")
                        <li class="list-group-item">
                            <b>VKontakte: </b> 
                            <a href="{{ $team->links->vkontakte }}">{{ $team->links->vkontakte }}</a>
                        </li>
                        @endif
                    @endif
                </ul>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection