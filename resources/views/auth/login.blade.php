<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="vk.com/mark_38_98">
    <meta name="author" content="MARK46">
    <link rel="icon" href="/favicon.ico">

    <title>Signin FamBox</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/login.css" rel="stylesheet">
</head>
<body>
<div class="login-bg"></div>
    <div class="container">
        <div class="modal-dialog">
            <div class="login-container">
                <h1>Login to Your Account</h1><br>

                <form id="login-form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <input id="email" type="text" name="email" required="" value="{{ old('email') }}"  placeholder="E-Mail">
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                        <input id="password" type="password" name="password" required="" placeholder="Password">
                    </div>

                    <input type="submit" class="login login-submit" value="Login">
                </form>
            </div>
        </div>
    </div>

    <!-- Javascript files-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
</body>
</html>
