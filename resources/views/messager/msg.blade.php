@extends('layouts.maket')

@section('content')
<div class="col-lg-12">
    <div class="daily-feeds card">
        <div class="card-header">
            <h3 class="h4">{{$header_text}}</h3>
        </div>
        <div class="card-body no-padding">
            @foreach($msg as $val)
            <div class="item clearfix">
                <div class="feed d-flex justify-content-between">
                    <div class="feed-body d-flex justify-content-between">
                        <a href="#" class="feed-profile"><img src="/img/user.png" alt="person" class="img-fluid rounded-circle"></a>
                        <div class="content">
                            <h5>{{ $val->id }} - {{$val->name}}</h5>
                            <span>{{$val->msg}}</span>
                            <div class="full-date"><small><b>{{$val->email}}</b></small></div>
                        </div>
                    </div>
                    <div class="date text-right"><small>{{$val->created_at}}</small></div>
                </div>
                @if($val->comment_id)
                <div class="quote has-shadow">
                    <p>The administration's response - <b>{{ App\User::find($val->comments->user_id)->name }}</b></p>
                    <small>{{$val->comments->comment}}</small>
                    <hr>
                    <div class="full-date"><small>{{$val->comments->created_at}}</small></div>
                </div>
                    @else
                    <div class="quote has-shadow">
                        <b id="inf">Comments is missing.</b>
                        <form action="/admin/msg/{{$val->id}}" style="display: none">
                            <div class="input-group">
                                <textarea id="answer{{ $val->id }}" placeholder="Your comments.." name="comment" rows="3" style="width: 100%"></textarea>
                            </div>
                            <br>
                            <div class="input-group ">
                                <input id="{{ $val->id }}" type="submit" class="btn btn-default" value="Send"  style="float: right;">
                            </div>
                        </form>
                        <div class="CTAs pull-right"><a href="#" class="btn btn-xs btn-secondary" onclick="showCommen(this)"><i class="fa fa-comment"> </i>Chat</a></div>
                    </div>
                @endif
            </div>
            @endforeach
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="/js/messager.js"></script>
@endsection