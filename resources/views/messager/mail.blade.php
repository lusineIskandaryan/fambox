<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>FamBox</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
            font-size: 100%;
            line-height: 1.6;
        }
        
        img {
            max-width: 100%;
        }
        
        body {
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            width: 100%!important;
            height: 100%;
        }
        
        a {
            color: #348eda;
        }
        
        .btn-primary {
            text-decoration: none;
            color: #FFF;
            background-color: #348eda;
            border: solid #348eda;
            border-width: 10px 20px;
            line-height: 2;
            font-weight: bold;
            margin-right: 10px;
            text-align: center;
            cursor: pointer;
            display: inline-block;
            border-radius: 25px;
        }
        
        .btn-secondary {
            text-decoration: none;
            color: #FFF;
            background-color: #aaa;
            border: solid #aaa;
            border-width: 10px 20px;
            line-height: 2;
            font-weight: bold;
            margin-right: 10px;
            text-align: center;
            cursor: pointer;
            display: inline-block;
            border-radius: 25px;
        }
        
        .last {
            margin-bottom: 0;
        }
        
        .first {
            margin-top: 0;
        }
        
        .padding {
            padding: 10px 0;
        }
        
        table.body-wrap {
            width: 100%;
            padding: 20px;
        }
        
        table.body-wrap .container {
            border: 1px solid #f0f0f0;
        }
        
        table.footer-wrap {
            width: 100%;
            clear: both!important;
        }
        
        .footer-wrap .container p {
            font-size: 12px;
            color: #666;
        }
        
        table.footer-wrap a {
            color: #999;
        }
        
        h1,
        h2,
        h3 {
            font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            line-height: 1.1;
            margin-bottom: 15px;
            color: #000;
            margin: 40px 0 10px;
            line-height: 1.2;
            font-weight: 200;
        }
        
        h1 {
            font-size: 36px;
        }
        
        h2 {
            font-size: 28px;
        }
        
        h3 {
            font-size: 22px;
        }
        
        p,
        ul,
        ol {
            margin-bottom: 10px;
            font-weight: normal;
            font-size: 14px;
        }
        
        ul li,
        ol li {
            margin-left: 5px;
            list-style-position: inside;
        }
        
        .container {
            display: block!important;
            max-width: 600px!important;
            margin: 0 auto!important;
            clear: both!important;
        }
        
        .body-wrap .container {
            padding: 20px;
        }
        
        .content {
            max-width: 600px;
            margin: 0 auto;
            display: block;
        }
        
        .content table {
            width: 100%;
        }
        
        .comment b {
            font-size: 11px;
            font-weight: bold;
        }
        
        .comment {
            display: -webkit-box;
            background: #f6f6f6;
            padding: 7px;
            font-size: 15px;
            margin: 10px 0px;
            box-shadow: 0 0 5px rgba(193, 193, 193, 0.62);
        }
        
        .title {
            font-size: 20px;
            display: block;
        }
    </style>
</head>

<body bgcolor="#f6f6f6">
    <table class="body-wrap">
        <tr>
            <td></td>
            <td class="container" bgcolor="#FFFFFF">
                <div class="content">
                    <table>
                        <tr>
                            <td>
                                <p>Hi {{ $user }}!</p>
                                <p>Administration <b>FamBox</b> replied to your message.</p>
                                <hr>
                                <span class="title">The administration's response.</span>
                                <span class="comment">
									Name: {{ $admin }}<br>
									{{ $admin_comment }}<br>
									<b>{{ $admin_created_at }}</b>
								</span>
                                <span class="date"></span>
                                <hr>
                                <span class="title">Your message.</span>
                                <span class="comment">
                                    Name: {{ $user }}<br>
                                    {{ $user_comment }}<br>
                                    <b>{{ $user_created_at }}</b>
                                </span>
                                <table>
                                    <tr>
                                        <td class="padding">
                                            <p><a href="https://fambox.x" class="btn-primary">Go to the website.</a></p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td></td>
        </tr>
    </table>
    <table class="footer-wrap">
        <tr>
            <td></td>
            <td class="container">
                <div class="content">
                    <table>
                        <tr>
                            <td align="center">
                                <p>
                                    Разработано знатоками своего дела Designed by
                                    <a href="#">
                                        <unsubscribe>FamBox</unsubscribe>
                                    </a>.
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td></td>
        </tr>
    </table>
</body>

</html>