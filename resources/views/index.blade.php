<!doctype html>

<html lang="en">

<meta charset="utf-8">
<title>Fambox</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<meta content="" name="keywords">
<meta content="" name="description">

<!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
<meta property="og:title" content="">
<meta property="og:image" content="">
<meta property="og:url" content="">
<meta property="og:site_name" content="">
<meta property="og:description" content="">

<!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="">
<meta name="twitter:title" content="">
<meta name="twitter:description" content="">
<meta name="twitter:image" content="">

<base href="/">

<!--Place your favicon.ico and apple-touch-icon.png in the template root directory -->
<link href="favicon.ico" rel="shortcut icon">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">

<!-- Bootstrap CSS File -->
<link href="project/assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Libraries CSS Files -->
<link href="project/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="project/assets/lib/animate-css/animate.min.css" rel="stylesheet">

<!-- Main Stylesheet File -->
<link href="project/assets/css/style.css" rel="stylesheet">

</head>
<body>
<app-root></app-root>
<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

<!--Required JavaScript Libraries-->
<script src="project/assets/lib/jquery/jquery.min.js"></script>
<script src="project/assets/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="project/assets/lib/superfish/hoverIntent.js"></script>
<script src="project/assets/lib/superfish/superfish.min.js"></script>
<script src="project/assets/lib/morphext/morphext.min.js"></script>
<script src="project/assets/lib/wow/wow.min.js"></script>
<script src="project/assets/lib/stickyjs/sticky.js"></script>
<script src="project/assets/lib/easing/easing.js"></script>

<!-- Template Specisifc Custom Javascript File -->
<script src="project/assets/js/custom.js"></script>

<!-- Please add other script sources before this section -->
					<!-- Section start -->
<script type="text/javascript" src="project/inline.bundle.js"></script>
<script type="text/javascript" src="project/polyfills.bundle.js"></script>
<script type="text/javascript" src="project/styles.bundle.js"></script>
<script type="text/javascript" src="project/vendor.bundle.js"></script>
<script type="text/javascript" src="project/main.bundle.js"></script></body>
					<!-- Section end 	-->
</body>
</html>
