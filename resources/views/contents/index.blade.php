@extends('layouts.maket')

@section('content')
<div class="col-lg-12">
    <div class="daily-feeds card">
        <div class="card-header">
            <h3 class="h4">Contents Manager</h3>
        </div>
        <div class="card-body">
            @if(!empty(Session::get('flash_message')))
                <div class="alert alert-success" role="alert">{{ Session::get('flash_message')}} </div>
            @endif
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th width="30%">Title</th>
                        <th width="70%">Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($contents as $content)
                    <tr>
                        <th scope="row">{{ $content->id }}</th>
                        <td>{{ $content->section_title }}</td>
                        <td class="text">{{ $content->section_description }}</td>
                        <td>
                            <div class="btn-group btn-group-justified" role="group">
                                <div class="btn-group" role="group">
                                    <a href="/admin/content/{{ $content->id }}" class="btn btn-xs btn-success"> Show </a>
                                </div>
                                <div class="btn-group" role="group">
                                    <a href="/admin/content/{{ $content->id }}/edit" class="btn btn-xs btn-success"> Edit </a>
                                </div>
                                <div class="btn-group" role="group">
                                    <form action="/admin/content/{{ $content->id }}" method="POST">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="submit" class="btn btn-xs btn-danger" 
                                        onclick="return confirm('Confirm delete?')" value="Delete">
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $contents->appends(['search' => Request::get('search')])->render() !!}
        </div>
    </div>
</div>
@endsection
