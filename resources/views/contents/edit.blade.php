@extends('layouts.maket')

@section('content')
<div class="col-lg-12">
    <div class="daily-feeds card">
        <div class="card-header">
            <h3 class="h4">Editing Portfolio</h3>
        </div>
        <div class="card-body ">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form enctype="multipart/form-data" action="/admin/content/{{ $content->id }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label class="form-control-label">Section Title</label>
                            <input type="text" name="section_title" placeholder="Section Titl" value="{{ $content->section_title }}" class="form-control form-control-sm">
                        </div>
                        <br>
                        <div class="form-group">
                            <label class="form-control-label">Section Description</label>
                            <textarea id="textarea1" rows="3" type="text" name="section_description" placeholder="Section Content.."
                                class="form-control form-control-sm">{{ $content->section_description }}</textarea>
                        </div>
                        <br>

                        <div class="form-group">
                            <label class="form-control-label">Title</label>
                            <input type="text" name="title" placeholder="Title" value="{{ $content->title }}" class="form-control form-control-sm">
                        </div>
                        <br>
                        <div class="form-group">
                            <label class="form-control-label">Content</label>
                            <textarea id="textarea2" rows="3" type="text" name="content" placeholder="Content.."
                                class="form-control form-control-sm">{{ $content->content }}</textarea>
                        </div>

                        <br>
                        <div class="form-group">
                            <label class="form-control-label">Menu</label><br>
                            <select class="form-control" name="menu_id" id="">
                                @foreach($menu as $_menu)
                                    <option value="{{$_menu->id}}">{{$_menu->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <br>
                        <div class="form-group">
                            <img id="img-avatar" class="rounded-circle" src="/images/{{ $content->image->name }}" width="20%" alt="default"><br><br>
                            <label class="btn btn-default btn-file" style="width: 100%;">
                                Select image...<input type="file" accept="image/*,image/jpeg" style="display: none;" name="image">
                            </label>
                            <a id="clear-btn" href="#" class="btn btn-danger" style="width: 100%;">Remove this image</a>
                        </div>
                        <br>
                        <div class="line"> </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <a href="/admin/content/" class="btn btn-secondary">Cancel</a>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
