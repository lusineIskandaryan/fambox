
@extends('layouts.maket')

@section('content')
    <div class="col-lg-12">
        <div class="daily-feeds card">
            <div class="card-header">
                <h3 class="h4">Subscribe Manager</h3>
            </div>
            <div class="card-body">
                @if(is_string($inf))
                    <div class="alert alert-success" role="alert">{{ $inf }}</div>
                @endif
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>E-Mail</th>
                        <th>Created date</th>
                        <th>Updatet date</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($subscribes as $value)
                        <tr>
                            <th scope="row">{{ $value->id }}</th>
                            <td>{{ $value->email }}</td>
                            <td>{{ $value->created_at }}</td>
                            <td>{{ $value->updated_at }}</td>
                            <td>
                                <div class="btn-group" role="group">
                                    <form action="/admin/subscribe/{{ $value->id }}" method="POST" id="formName">
                                        <input type="hidden" name="_method" value="PUT">
                                        <input type="hidden" name="status" value="{{ $value->status == 0 ? 1 : 0 }}">

                                        <input type="checkbox" name="checkbox1" id="checkbox1" class="ios-toggle"
                                               {{ $value->status != 0 ? 'checked' : null }}
                                               onclick="document.getElementById('formName').submit()"/>
                                        <label for="checkbox1" class="checkbox-label" data-off="Deactivate" data-on="Active"></label>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $subscribes->appends(['search' => Request::get('search')])->render() !!}
            </div>
        </div>
    </div>
@endsection
@section('script')
@endsection