import { Component, OnInit } from '@angular/core';
import { AboutService } from './about.service';
import { AboutContent } from '../share/share';
import {  Observable } from 'rxjs/Observable'

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  private aboutContents: AboutContent[] = [];
  constructor(private service: AboutService) { }
  ngOnInit() {
    this.getRequest()
    console.log(this.aboutContents)
  }
  getRequest(): Observable<any> {
    this.service.getAbout()
      .map(response => response.json())
      .subscribe((response) => {
      console.log(this.aboutContents + " : " + response);
        // this.aboutContents = <AboutComponent[]>response;
            return response;
          //  this.aboutContents = response;
          // console.log(this.aboutContents + " : " + response);
            // return this.aboutContents;
        });
       console.log(this.aboutContents);
  }
}
