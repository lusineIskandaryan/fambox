import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AboutContent } from '../share/share';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class AboutService {
    private url = 'http://127.0.0.1:8000/api/about';
    constructor (private http: HttpClient){}
    getAbout(){
        const headers = new HttpHeaders().set("content-type", "application/json");
        return this.http.get(this.url, {headers:headers});
    }
}