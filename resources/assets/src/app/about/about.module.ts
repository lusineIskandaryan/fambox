import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AboutComponent } from './about.component';
import { AboutRoutingModule } from './about.routing.module';
import { AboutService } from './about.service';
 
@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    AboutRoutingModule
  ],
  declarations: [ AboutComponent
    
  ],
  exports: [ AboutComponent ],
  providers: [ AboutService ],
})
export class AboutModule { }

