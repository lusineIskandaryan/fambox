import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PortfolioComponent } from './portfolio.component';
import { PortfolioRoutingModule } from './portfolio.routing.module'
 
@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    PortfolioRoutingModule
  ],
  declarations: [ PortfolioComponent
    
  ],
  exports: [ PortfolioComponent ],
  providers: [ ],
})
export class PortfolioModule { }


