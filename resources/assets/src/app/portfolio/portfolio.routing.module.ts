import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PortfolioComponent } from './portfolio.component';

const portfolioRoutes: Routes = [
    {
        path: '', component: PortfolioComponent 
    }
];
@NgModule({
    imports: [ 
      RouterModule.forChild (portfolioRoutes)
    ],
    exports: [ RouterModule ]
})

export class PortfolioRoutingModule {}
