import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeamComponent } from './team.component';

const teamRoutes: Routes = [
    {
        path: '', component: TeamComponent 
    }
];
@NgModule({
    imports: [ 
      RouterModule.forChild (teamRoutes)
    ],
    exports: [ RouterModule ]
})

export class TeamRoutingModule {}
