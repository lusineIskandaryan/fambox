import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TeamComponent } from './team.component';
import { TeamRoutingModule } from './team.routing.module';
 
@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    TeamRoutingModule
  ],
  declarations: [ TeamComponent
    
  ],
  exports: [ TeamComponent ],
  providers: [ ],
})
export class TeamModule { }

