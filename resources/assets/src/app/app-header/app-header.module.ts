import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppHeaderComponent } from './app-header.component';
import { RouterModule } from '@angular/router';
 
@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    RouterModule
  ],
  declarations: [ AppHeaderComponent
    
  ],
  exports: [ AppHeaderComponent ],
  providers: [ ],
})
export class AppHeaderModule { }

