import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TestimonialsComponent } from './testimonials.component';

const testimonialsRoutes: Routes = [
    {
        path: '', component: TestimonialsComponent 
    }
];
@NgModule({
    imports: [ 
      RouterModule.forChild (testimonialsRoutes)
    ],
    exports: [ RouterModule ]
})

export class TestimonialsRoutingModule {}
