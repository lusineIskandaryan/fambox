import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TestimonialsComponent } from './testimonials.component';
import { TestimonialsRoutingModule } from './testimonials.routing.module'
 
@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    TestimonialsRoutingModule
  ],
  declarations: [ TestimonialsComponent
    
  ],
  exports: [ TestimonialsComponent ],
  providers: [ ],
})
export class TestimonialsModule { }


