import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";

const routes : Routes =[
	{ path : '' , redirectTo: '/main', pathMatch: 'full' },
    { path : 'main' , loadChildren: 'app/main-page/main-page.module#MainPageModule'},
	{ path : 'about' , loadChildren: 'app/about/about.module#AboutModule' },
    { path : 'services' , loadChildren: 'app/our-services/our-services.module#OurServicesModule' },
    { path : 'portfolio' , loadChildren: 'app/portfolio/portfolio.module#PortfolioModule' },
    { path : 'testimonials' , loadChildren: 'app/testimonials/testimonials.module#TestimonialsModule' },
    { path : 'team' , loadChildren: 'app/team/team.module#TeamModule' },
    { path : 'contact' , loadChildren: 'app/contact-us/contact-us.module#ContactUsModule' },
	{ path : '**' , component: PageNotFoundComponent}
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
