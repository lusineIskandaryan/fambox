import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppFooterComponent } from './app-footer.component';
 
@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule
  ],
  declarations: [ AppFooterComponent
    
  ],
  exports: [ AppFooterComponent ],
  providers: [ ],
})
export class AppFooterModule { }

