import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MainPageComponent } from './main-page.component';
import { MainPageRoutingModule } from './main-page.routing.module';
import { AboutModule } from '../about/about.module';
import { OurServicesModule } from '../our-services/our-services.module';
import { PortfolioModule } from '../portfolio/portfolio.module';
import { TestimonialsModule } from '../testimonials/testimonials.module';
import { TeamModule } from '../team/team.module';
import { ContactUsModule } from '../contact-us/contact-us.module';
 
@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    MainPageRoutingModule,
    AboutModule,
    OurServicesModule,
    PortfolioModule,
    TestimonialsModule,
    TeamModule,
    ContactUsModule
  ],
  declarations: [ MainPageComponent
    
  ],
  exports: [ MainPageComponent ],
  providers: [ ],
})
export class MainPageModule { }

