import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPageComponent } from './main-page.component';

const mainPageRoutes: Routes = [
    {
        path: '', component: MainPageComponent 
    }
];
@NgModule({
    imports: [ 
      RouterModule.forChild (mainPageRoutes)
    ],
    exports: [ RouterModule ]
})

export class MainPageRoutingModule {}
