import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OurServicesComponent } from './our-services.component';

const ourServicesRoutes: Routes = [
    {
        path: '', component: OurServicesComponent 
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ourServicesRoutes)
    ],
    exports: [ RouterModule ]
})
export class OurServicesRoutingModule {}
