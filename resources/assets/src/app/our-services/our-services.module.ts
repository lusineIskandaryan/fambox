import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { OurServicesComponent } from './our-services.component';
import { OurServicesRoutingModule } from './our-services.routing.module'

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    OurServicesRoutingModule
  ],
  declarations: [ OurServicesComponent
    
  ],
  exports: [ OurServicesComponent ],
  providers: [ ],
})
export class OurServicesModule { }


