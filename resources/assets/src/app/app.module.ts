import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app.routing-module';
import { MainPageModule } from './main-page/main-page.module';
import { AppFooterModule } from './app-footer/app-footer.module';
import { AppHeaderModule } from './app-header/app-header.module';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    MainPageModule,
    AppFooterModule,
    AppHeaderModule
    ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
