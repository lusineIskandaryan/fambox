import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ContactUsComponent } from './contact-us.component';
import { ContactUsRoutingModule } from './contact-us.routing.module';
 
@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ContactUsRoutingModule
  ],
  declarations: [ ContactUsComponent
    
  ],
  exports: [ ContactUsComponent ],
  providers: [ ],
})
export class ContactUsModule { }

