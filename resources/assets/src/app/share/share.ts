export class AboutContent {
    public section_title?: string;
    public section_description?: string;
    public title?: string;
    public content?: string;
    public image?: Image;
    constructor() {}
}

export class Image {
    public path?: string;
    constructor(path) {}
}
