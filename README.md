# ========================================================================================================================
# FamBox admin panel documentation!
# ========================================================================================================================
# Consoles commands

#   1) First create a database "fambox_new"!
    2) Migration all of a database table.               - php artisan migrate
    3) Generation of fake data in the database tables.  - php artisan db:seed
        - Username and password for the test account.
#           - username: test@email.com
#           - password: 123456

    4) Start the server.                                - php artisan server
#       - The server is available here: http://127.0.0.1:8000/


# ======= URL's ==========================================================================================================

MENU Items      - http://127.0.0.1:8000/api/menue

ABOUT US        - http://127.0.0.1:8000/api/page/contents/1
PORTFOLIO       - http://127.0.0.1:8000/api/page/portfolios
TESTIMONIALS    - http://127.0.0.1:8000/api/page/testimonials
OUR TEAM        - http://127.0.0.1:8000/api/page/teams
OUR SERVICES    - http://127.0.0.1:8000/api/page/services


# ========================================================================================================================
# API Info

    Get all pages..

    0) Method [ GET ]   |    Url [ http://127.0.0.1:8000/api/menue ]    | Status code [ success: 200, error: 404 ]
    1) Method [ GET ]   |    Url [ http://127.0.0.1:8000/api/page ]     | Status code [ success: 200, error: 404 ]
    
    
    # Example code (JavasSript jQuery)

        $.ajax({
            crossDomain: true,
            url: "http://127.0.0.1:8000/api/page",
            method: "GET",
            success: function(response) {
                console.log(response);
            },
            error: function(e) {
                ...
            }
        });

        Result console log: 
        [{
            "id": 2,
            "title": "Title 1",
            "content": "Text 1",
            "created_at": null,
            "updated_at": null,
            "uri": "portfolios",
            "portfolios": [{
                "id": 2,
                "title": "Title 1",
                "content": "Text 1",
                "link": "www.*.*",
                "created_at": null,
                "updated_at": null,
                "image": {
                    "id": 1,
                    "path": "default.png"
                }
            },
            ...
            ]
        },
        ...
        ]
    
#    --------------

    Gets a specific resource by ID..

    2) Method [ GET ]   |    Url [ http://127.0.0.1:8000/api/page/portfolios ]          | Status code [ success: 200, error: 404 ]
    3) Method [ GET ]   |    Url [ http://127.0.0.1:8000/api/page/portfolios/2 ]        | Status code [ success: 200, error: 404 ]
    
    # Example code (JavasSript jQuery)

        $.ajax({
            crossDomain: true,
            url: "http://127.0.0.1:8000/api/page/portfolios", // Pages name
            method: "GET",
            success: function(response) {
                console.log(response);
            },
            error: function(e) {
                ...
            }
        });

        Result console the: 
        [{
            "id": 2,
            "title": "Title 1",
            "content": "Text 1",
            "created_at": null,
            "updated_at": null,
            "uri": "portfolios",
            "portfolios": [{
                "id": 2,
                "title": "Title 1",
                "content": "Text 1",
                "link": "www.*.*",
                "created_at": null,
                "updated_at": null,
                "image": {
                    "id": 1,
                    "path": "default.png"
                }
            }, 
            ...
            ]
        }]

        # ----------------------------

        $.ajax({
            crossDomain: true,
            url: "http://127.0.0.1:8000/api/page/portfolios/2", // Pages name and resource ID
            method: "GET",
            success: function(response) {
                console.log(response);
            },
            error: function(e) {
                ...
            }
        });

        Result console the: 
        [{
            "id": 2,
            "title": "Title 1",
            "content": "Text 1",
            "created_at": null,
            "updated_at": null,
            "uri": "portfolios",
            "portfolios": [{
                "id": 2,
                "title": "Title 1",
                "content": "Text 1",
                "link": "www.*.*",
                "created_at": null,
                "updated_at": null,
                "image": {
                    "id": 1,
                    "path": "default.png"
                }
            }]
        }]

#    --------------------------------------------------------------------------------------------------------------------
    
    1) Method [ POST ]   |    Url [ http://127.0.0.1:8000/api/email ]          | Status code [ success: 201, error: 404 ]

    # Example code (JavasSript jQuery)

        $.ajax({
            crossDomain: true,
            url: "http://127.0.0.1:8000/api/email",
            method: "POST",
            headers: {
                "content-type": "application/json",
            },
            processData: false,
            data: JSON.stringify({ 
                "name":     " /* input value */ ", 
                "email":    " /* input value */ ", 
                "subject":  " /* input value */ ",
                "message":  " /* input value */ "
            }),
            success: function(response) {
                console.log(response);
            },
            error: function(e) {
                ...
            }
        });

        Success result console the:     [ { "Message": "OK" } ]
        Error result console the:       [ { "Message": "Validator error.." } ]
            - This error occurs when fields validation.

#    --------------------------------------------------------------------------------------------------------------------
    
    2) Method [ POST ]   |    Url [ http://127.0.0.1:8000/api/subscribe ]       | Status code [ success: 200, error: 404 ]

    # Example code (JavasSript jQuery)

        $.ajax({
            crossDomain: true,
            url: "http://127.0.0.1:8000/api/subscribe",
            method: "POST",
            headers: {
                "content-type": "application/json",
            },
            processData: false,
            data: JSON.stringify({ 
                "email":    " /* input value */ "
            }),
            success: function(response) {
                console.log(response);
            },
            error: function(e) {
                ...
            }
        });

        Success result console the:     [ { "Message": "ok" } ]
        Success result console the:     [ { "Message": "This mail has been signed." } ]
        Error result console the:       [ { "Message": "error" } ]
            - This error occurs when fields validation.

# ========================================================================================================================
# E-Mail Settings

    - Open the file '.env' and following are the line change

        MAIL_DRIVER=smtp
        MAIL_HOST=smtp.gmail.com
        MAIL_PORT=587
    #   MAIL_USERNAME=example@gmail.com     <- Your E-Mail
    #   MAIL_PASSWORD=123456                <- Your E-Mail
        MAIL_ENCRYPTION=tls

# ========================================================================================================================
