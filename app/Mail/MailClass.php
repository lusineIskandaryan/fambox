<?php

namespace App\Mail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailClass extends Mailable
{
    use Queueable, SerializesModels;

    protected $settings;

    public function __construct($settings)
    {
        $this->settings = $settings;
    }

    public function build()
    {
        return $this->view('messager.mail', $this->settings)->subject('Your Reminder!');
    }

}
