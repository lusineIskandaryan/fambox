<?php

namespace App\Mail;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\SendQueuedMailable;
use Illuminate\Queue\SerializesModels;
use App\Model\Subscribe;
use Illuminate\Support\Facades\Mail;
use Validator;

class SenderAll
{
    public function send($title, $text, $date, $btn_name, $btn_url) {
        ini_set('max_execution_time', 0);
        $i = 0;
        $all = Subscribe::where('status', 1)->get();

        foreach ($all as $email) {
            if($i == 5) {
                # TIME OUT
                set_time_limit(3);
                # ...
                $i = 0;
            }
            $i++;

            # Sending E-Mail
            Mail::to($email)->send(new SubscribeClass([
                'email'         => $email->email,
                'created_at'    => $date,
                'title'         => $title,
                'text'          => $text,
                'btn'           => [
                    'name'  => $btn_name,
                    'url'   => $btn_url
                ]
            ]));
        }
    }
}
