<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $hidden = [
		'pivot',
		'image_id',
		'menu_id'
	];
	
	protected $fillable = [
		'section_title', 
		'section_description',
		'title', 
		'image_id', 
		'content', 
		'menu_id'
	];
	
	public $rules = [
		'section_title' 		=> 'required|max:255',
		'section_description' 	=> 'required',
		'title' 				=> 'max:255',
		'image' 				=> 'mimes:jpeg,bmp,png',
		'content' 				=> '',
		'menu_id' 				=> 'required|numeric'
	];
	
	public function menus() {
		return $this->hasOne(\App\Model\Menu::class, 'id', 'menu_id');
	}

	public function image() {
		return $this->hasOne(\App\Model\Other\Image::class, 'id', 'image_id');
	}

	public function service() {
		return $this->hasMany(\App\Model\Service::class, 'content_id', 'id');
	}

	public function team() {
		return $this->hasMany(\App\Model\Team::class, 'content_id', 'id');
	}

	public function portfolio() {
		return $this->hasMany(\App\Model\Portfolio::class, 'content_id', 'id');
	}

	public function testimonial() {
		return $this->hasMany(\App\Model\Testimonial::class, 'content_id', 'id');
	}
}