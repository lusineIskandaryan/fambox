<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $hidden = [
		'pivot',
		'image_id'
	];
	
	protected $fillable = [
		'name', 
		'content',
		'read_more_url', 
		'position', 
		'is_active', 
		'image_id', 
		'status', 
		'content_id'
	];
	
	public $rules = [
		'name' 			=> 'required|max:255', 
		'content' 		=> 'required',
		'read_more_url'	=> 'required|max:255',
		'position'		=> 'required|max:255', 
		'is_active'		=> 'numeric', 
		'image'			=> 'mimes:jpeg,bmp,png', 
		'status'		=> 'numeric', 
		'content_id'	=> 'required|numeric'
	];
	
	
    public function image() {
        return $this->hasOne("App\Model\Other\Image", 'id', 'image_id');
    }
}
