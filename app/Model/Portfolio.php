<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
	protected $hidden = [
		'pivot', 
		'image_id'
	];
	
	protected $fillable = [
		'title', 
		'content',
		'link', 
		'image_id', 
		'status', 
		'content_id'
	];
	
	public $rules = [
		'title' 		=> 'required|max:255', 
		'content' 		=> 'required',
		'link'			=> 'required|max:255', 
		'image'			=> 'mimes:jpeg,bmp,png', 
		'status'		=> 'numeric', 
		'content_id'	=> 'required|numeric'
	];
	
    public function image() {
        return $this->hasOne("App\Model\Other\Image", 'id', 'image_id');
    }
}
