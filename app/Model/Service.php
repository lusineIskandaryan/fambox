<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $hidden = [
		'pivot'
	];
	
	protected $fillable = [
		'title', 
		'content',
		'link', 
		'status', 
		'content_id'
	];
	
	public $rules = [
		'title' 		=> 'required|max:255', 
		'content' 		=> 'required',
		'link'			=> 'required|max:255', 
		'status'		=> 'numeric', 
		'content_id'	=> 'required|numeric'
	];
	
}
