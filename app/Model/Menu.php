<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = "menus";
    public $timestamps = false;
    protected $hidden = ['pivot', 'parent_id', 'ordering'];
    

    public function sub() {
        return $this->hasMany(\App\Model\Menu::class, 'parent_id', 'id');
    }
}
