<?php

namespace App\Model\Other;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = "images";
    public $timestamps = false;
	
	protected $fillable = [
		'path', 
		'md5_hash', 
		'name'
	];
}
