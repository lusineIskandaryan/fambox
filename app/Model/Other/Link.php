<?php

namespace App\Model\Other;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    public $timestamps = false;
}
