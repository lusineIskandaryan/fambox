<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $hidden = [
		'pivot',
		'image_id'
	];
	
	protected $fillable = [
		'name', 
		'position',
		'image_id', 
		'link_id', 
		'status', 
		'content_id'
	];
	
	public $rules = [
		'name' 			=> 'required|max:255', 
		'position' 		=> 'required|max:255',
		'image'			=> 'mimes:jpeg,bmp,png', 
		'status'		=> 'numeric', 
		'content_id'	=> 'required|numeric'
	];
	
	
	
    public function image() {
        return $this->hasOne("App\Model\Other\Image", 'id', 'image_id');
    }
    public function links() {
        return $this->hasOne("App\Model\Other\Link", 'id', 'link_id');
    }
}
