<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Other\Template;
use Validator;
class MailTemplate extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (view()->exists('template/index')) {
            $template = Template::all();
            $data = [
                'title' => 'temple',
                'template' => $template
            ];
            return view('template/index', $data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('template/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/admin/template/create')->withErrors($validator);
        }

        $template = new Template;
        $template->name = $request->input('name');
        $template->content = $request->input('content');
        $template->save();
        return redirect('admin/template/'.$template->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $template = Template::findOrFail($id);
        return view('template/show', [
            'title' => 'Template',
            'template' => $template
        ]);
    }

    public function preview($id)
    {
        $template = Template::findOrFail($id);
        return view('template/view', [
            'title' => 'Template',
            'template' => $template
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $template = Template::findOrFail($id);
        return view('template/edit', [
            'title' => 'Template',
            'template' => $template
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/admin/template/' . $id . '/edit')->withErrors($validator);
        }

        $template = Template::findOrFail($id);
        $template->name = $request->input('name');
        $template->content = $request->input('content');
        $template->save();
        return redirect('admin/template/'.$template->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Template::destroy($id);
        return redirect('/admin/template');
    }
}
