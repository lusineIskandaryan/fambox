<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Content;
use App\Model\Other\Image;
use App\Model\Menu;
use \Session;
use \Validator;
use App\Mail\SenderAll;

class ContentsController extends Controller
{
    public function api($item, $id = false)
    {
        if ($id) {
            switch ($item) {
                case 'service':
                    return response(\App\Model\Service::findOrFail($id), 200);
                break;
                case 'testimonial':
                    return response(\App\Model\Testimonial::findOrFail($id), 200);
                break;
                case 'portfolio':
                    return response(\App\Model\Portfolio::findOrFail($id), 200);
                break;
                case 'team':
                    return  response(\App\Model\Team::findOrFail($id), 200);
                break;
            }
        } else {
            $contents = Content::with('service', 'testimonial', 'portfolio', 'team', 'image', 'menus')->whereHas('menus', function ($q) use ($item) {
                $q->where('uri', $item);
            })->get();
        }

        if (!sizeof($contents)) {
            return response(['message' => 'Not found..'], 404);
        }
        return response($contents, 200);
    }


    public function index()
    {
        $contents = Content::paginate(10);
        return view('contents.index', ['contents' => $contents]);
    }

    public function create()
    {
        $menu = Menu::all();
        return view('contents.create', ['menu' => $menu]);
    }

    public function show($id)
    {
        $content = Content::with('image', 'menus')->findOrFail($id);
        return view('contents.show', compact("content"));
    }

    public function edit($id)
    {
        $menu = Menu::all();
        $content = Content::with('image', 'menus')->findOrFail($id);
        return view('contents.edit', ['menu' => $menu, "content" => $content]);
    }

    public function store(Request $request)
    {
        $content = new Content;
        $validator = Validator::make($request->all(), $content->rules);

        if ($validator->fails()) {
            return redirect('/admin/content/create')->withErrors($validator);
        }

        $_image = new Image;
        if (($img_res = (new Controller)->UploadImage($request, "image"))) {
            $_image->name               = $img_res;
        }
        $_image->save();

        $content->section_title         = $request->input('section_title');
        $content->section_description   = $request->input('section_description');
        $content->title                 = $request->input('title');
        $content->content               = $request->input('content');
        $content->menu_id               = $request->input('menu_id');
        $content->image_id              = $_image->id;
        $content->save();

        # EMAIL SUBSCRIBES
        $_SENDERALL = new SenderAll();
        $_SENDERALL->send(
            "New information on the site FamBox.",
            "The page had added a new portfolio ($content->title)..",
            $content->updated_at,
            "Go to the contents page.",
            "/content");

        Session::flash('flash_message', 'Content Successfully created!');
        return redirect('/admin/content/');
    }

    public function update(Request $request, $id)
    {
        $content = Content::findOrFail($id);
        $validator = Validator::make($request->all(), $content->rules);

        if ($validator->fails()) {
            return redirect("/admin/content/$id/edit")->withErrors($validator);
        }

        $_image = Image::find($id);
        if ($_image) {
            if (($img_res = (new Controller)->UploadImage($request, "image"))) {
                $_image->name               = $img_res;
            }
            $_image->save();
            $content->image_id              = $_image->id;
        }

        $content->section_title         = $request->input('section_title');
        $content->section_description   = $request->input('section_description');
        $content->title                 = $request->input('title');
        $content->content               = $request->input('content');
        $content->menu_id               = $request->input('menu_id');
        $content->save();

        Session::flash('flash_message', 'Content Successfully updated!');
        return redirect('/admin/content/');
    }

    public function destroy($id)
    {
        Content::destroy($id);
        Session::flash('flash_message', 'Content deleted!');
        return redirect('/admin/content/');
    }
}
