<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Menu;
use \Session;
use \Validator;

class MenuController extends Controller
{
    public function index()
    {
        $menue = Menu::all();
        return view('menue.index', ['menue' => $menue]);

    }

    public function create()
    {
        // $query = Menu::where(function ($query) {
        //     $query->whereIn('parent_id', [1, 2])->orWhereNull('parent_id');
        // })->get();

        $menues = Menu::all();
        return view('menue.create', [ 'menue' => $menues]);
    }

    public function show($id)
    {
      return redirect('admin/menue');
    }

    public function edit($id)
    {

        $menues = Menu::all();
        
        $menue = Menu::findOrFail($id);
        return view('menue.edit', ["menu" => $menue, 'menue' => $menues]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required|max:255',
            'uri'       => 'required',
            'ordering'  =>'required|numeric',
            'parent_id' =>'required|numeric'
        ]);

        if ($validator->fails()) {
            return redirect('/admin/menue/create')->withErrors($validator);
        }

        $services = new Menu;
        $services->name         = $request->input('name');
        $services->uri          = $request->input('uri');
        $services->ordering     = $request->input('ordering');
        $services->parent_id    = ($request->input('parent_id') == 0 ? NULL : $request->input('parent_id'));
        $services->save();

        Session::flash('flash_message', 'Menu Successfully created!');
        return redirect('/admin/menue/');
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required|max:255',
            'uri'       => 'required',
            'ordering'  =>'required|numeric',
            'parent_id' =>'numeric'
        ]);

        if ($validator->fails()) {
            return redirect("/admin/menue/$id/edit")->withErrors($validator);
        }

        $services = Menu::findOrFail($id);
        $services->name         = $request->input('name');
        $services->uri          = $request->input('uri');
        $services->ordering     = $request->input('ordering');
        $services->parent_id    = $request->input('parent_id');
        $services->save();

        Session::flash('flash_message', 'Menu Successfully updated!');
        return redirect('/admin/menue/index');
    }

    public function destroy($id)
    {
        Menu::destroy($id);
        Session::flash('flash_message', 'Service deleted!');
        return redirect('/admin/menue/');
    }
    public function api()
    {
        $menu = Menu::whereNull('parent_id')->with(array('sub' => function($query) { 
             $query->with(array('sub' => function($query) { 
                 $query->with('sub')->orderBy('ordering'); 
             }))->orderBy('ordering'); 
        }))->orderBy('ordering')->get();
        return $menu;
    }
}
