<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\SendQueuedMailable;
use Illuminate\Queue\SerializesModels;
use App\Model\Subscribe;
use Illuminate\Support\Facades\Mail;
use Validator;
use DB;

use App\Mail\SubscribeClass;

class SubscribesController extends Controller
{
    public function index($inf = null) {
        if (view()->exists('subscribes.index')) {
            $subscribes = Subscribe::paginate(10);
            $data = [
                'inf' => $inf,
                'subscribes' => $subscribes
            ];
            return view('subscribes.index', $data);
        }
    }

    public function update(Request $request, $id) {
        $status = $request->input('status');

        $validator = Validator::make(
            [ 'status' => $status],
            [ 'status' => 'required|regex:/(^([0-1]))/u' ]
        );

        if ($validator->fails()) {
            $status = 1;
        }

        $subscribes = Subscribe::findOrFail($id);
        $subscribes->status = $status;
        $subscribes->link = $this->mail($subscribes->email, $subscribes->status, $subscribes->updated_at);
        $subscribes->save();
        
        return redirect('/admin/subscribe');
    }

    public function api(Request $request) {
        $this->validate($request, [
            'email' => 'required|email'
        ]);

        $email = $request->input('email');

        if(Subscribe::where('email', $email)->first()) {
            return response(['message' => 'This mail has been signed.'], 200);
        }

        $subscribes = new Subscribe;
        $subscribes->email = $email;
        $subscribes->link = $this->mail($email, $subscribes->status, $subscribes->updated_at);

        return $subscribes->save() ? response(['message' => 'ok'], 200) : response(['message' => 'error'], 404);
    }

    public function status($id, $status) {
        if(($subscribes = Subscribe::where('link', $id)->first())) {
            $subscribes->status = $status;
            $subscribes->link = $this->mail($subscribes->email, $status, $subscribes->updated_at);
            $subscribes->save();            
        }
        return redirect("/");
    }

    private function mail($email, $status, $date) {

        $cfg = [
            'text' => [
                'You have successfully subscribed to news.<br>If you want to unsubscribe, click below "Unsubscribe".',
                'You have successfully unsubscribed from the news. If you want to subscribe click the button below "Subscribe".'
            ],
            'name' => [
                'Unsubscribe',
                'Subscribe'
            ]
        ];

        $revers_status = $status == 0 ? 1 : 0;
        $link_key = md5(bcrypt('123'));

        Mail::to($email)->send(new SubscribeClass([
            'email' => $email,
            'created_at' => $date,
            'title' => 'Information about the subscription!',
            'text' => $cfg['text'][$revers_status],
            'btn' => [
                'name' => $cfg['name'][$revers_status], 
                'url' => "http://127.0.0.1:8000/subscribe/$link_key/$revers_status"
            ]
        ]));

        return $link_key;
    }
}
