<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Model\Other\Image;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function UploadImage($request, $name) {
        if ($request->hasFile($name)) {
            $image = $request->file($name);
            $md5 = mb_strtoupper(md5_file($image));

            // $db_img = Image::where("md5_hash", $md5)->first();
            // if(sizeof($db_img) != 0)
            //     return ['id' => $db_img->id];

            $dir = public_path("\\images\\");
            if (!\File::exists($dir)) {
                \File::makeDirectory($dir, $mode = 0777, true, true);
            }

            $extension = $request->file($name)->getClientOriginalExtension();
            $image_name = str_random(10) . "." . $extension;
            if ($image->move($dir, $image_name)) {
                return $image_name;
                // return ['name' => $image_name, 'path' => "\\images\\" . $image_name, 'md5' => $md5];
            }
        }
        return false;
    }
}
