<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Model\Content;
use App\Model\Testimonial;
use App\Model\Other\Image;
use \Session;
use \Validator;
use App\Mail\SenderAll;

class TestimonialsController extends Controller
{
    public function index()
    {
        $servicess = Testimonial::paginate(10);
        return view('testimonials.index', ['testimonials' => $servicess]);
    }

    public function create()
    {
        $contents = Content::all();
        return view('testimonials.create', ["contents" => $contents]);
    }

    public function show($id)
    {
        return redirect('/admin/testimonial/');
    }

    public function edit($id)
    {
        $contents = Content::all();
        $testimonials = Testimonial::findOrFail($id);
        return view('testimonials.edit', ["testimonials" => $testimonials, "contents" => $contents]);
    }

    public function store(Request $request)
    {
        $testimonial = new Testimonial;
        $validator = Validator::make($request->all(), $testimonial->rules);
        
        if ($validator->fails()) {
            return redirect('/testimonial/create')->withErrors($validator);
        }

        $_image = new Image;
        if(($img_res = (new Controller)->UploadImage($request, "image"))) {
            $_image->name           = $img_res;
        }
        $_image->save();

        $testimonial->name          = $request->input('name');
        $testimonial->content       = $request->input('content');
        $testimonial->read_more_url = $request->input('read_more_url');
        $testimonial->position      = $request->input('position');
        $testimonial->is_active     = $request->input('is_active');
        $testimonial->status        = $request->input('status');
        $testimonial->content_id    = $request->input('content_id');
        $testimonial->image_id      = $_image->id;
        $testimonial->save();

        $_SENDERALL = new SenderAll();
        $_SENDERALL->send(
            "New information on the site FamBox.",
            "The page had added a new testimonial ($testimonial->name)..",
            $testimonial->updated_at,
            "Go to the testimonials page.",
            "http://127.0.0.1:8000/testimonial");

        Session::flash('flash_message', 'Testimonial Successfully created!');
        return redirect('/admin/testimonial/');
    }

    public function update(Request $request, $id)
    {
        $testimonial = Testimonial::findOrFail($id);
        $validator = Validator::make($request->all(), $testimonial->rules);
        
        if ($validator->fails()) {
            return redirect("/admin/testimonial/$id/edit")->withErrors($validator);
        }
       
        $testimonial->name          = $request->input('name');
        $testimonial->content       = $request->input('content');
        $testimonial->read_more_url = $request->input('read_more_url');
        $testimonial->position      = $request->input('position');
        $testimonial->is_active     = $request->input('is_active');
        $testimonial->status        = $request->input('status');
        $testimonial->content_id    = $request->input('content_id');
        $testimonial->save();

        $_image = Image::findOrFail($testimonial->image_id);
        if(($img_res = (new Controller)->UploadImage($request, "image"))) {
            $_image->name           = $img_res;
        }
        $_image->save();

        $_SENDERALL = new SenderAll();
        $_SENDERALL->send(
            "New information on the site FamBox.",
            "The page had added a new testimonial ($testimonial->name)..",
            $testimonial->updated_at,
            "Go to the testimonials page.",
            "http://127.0.0.1:8000/testimonial");

        Session::flash('flash_message', 'Testimonial Successfully created!');
        return redirect('/admin/testimonial/');
    }

    public function destroy($id)
    {
        Testimonial::destroy($id);
        Session::flash('flash_message', 'Testimonial deleted!');
        return redirect('/admin/testimonial/');
    }
}
