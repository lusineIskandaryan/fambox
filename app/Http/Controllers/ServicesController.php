<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Content;
use App\Model\Service;
use App\Model\Other\Image;
use \Session;
use \Validator;
use App\Mail\SenderAll;

class ServicesController extends Controller
{
    public function index()
    {
        $services = Service::paginate(10);
        return view('services.index', ['services' => $services]);
    }

    public function create()
    {
        $contents = Content::all();
        return view('services.create', ["contents" => $contents]);
    }

    public function show($id)
    {
        $service = Service::with('image')->findOrFail($id);
        return view('services.show', compact("service"));
    }

    public function edit($id)
    {
        $contents = Content::all();
        $services = Service::findOrFail($id);
        return view('services.edit', ["service" => $services, "contents" => $contents]);
    }

    public function store(Request $request)
    {
        $services = new Service;
        $validator = Validator::make($request->all(), $services->rules);

        if ($validator->fails()) {
            return redirect('/admin/service/create')->withErrors($validator);
        }

        $services->title        = $request->input('title');
        $services->link         = $request->input('link');
        $services->content      = $request->input('content');
        $services->status       = $request->input('status');
        $services->content_id   = $request->input('content_id');
        $services->save();

        $_SENDERALL = new SenderAll();
        $_SENDERALL->send(
            "New information on the site FamBox.",
            "The page had added a new Service ($services->title)..",
            $services->updated_at,
            "Go to the services page.",
            "/services");

        Session::flash('flash_message', 'Services Successfully created!');
        return redirect('/admin/service/');
    }

    public function update(Request $request, $id)
    {
        $services = Service::findOrFail($id);
        $validator = Validator::make($request->all(), $services->rules);
        
        if ($validator->fails()) {
            return redirect("/admin/service/$id/edit")->withErrors($validator);
        }

        $services->title        = $request->input('title');
        $services->link         = $request->input('link');
        $services->content      = $request->input('content');
        $services->status       = $request->input('status');
        $services->content_id   = $request->input('content_id');
        $services->save();

        Session::flash('flash_message', 'Service Successfully updated!');
        return redirect('/admin/service/');
    }

    public function destroy($id)
    {
        Service::destroy($id);
        Session::flash('flash_message', 'Service deleted!');
        return redirect('/admin/service/');
    }
}
