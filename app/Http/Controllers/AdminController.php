<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Model\Other\Image;
use Validator;

class AdminController extends Controller
{
    public function index($inf = null)
    {
        $users = User::with('image')->paginate(10);
        return view('admins.index', ["users" => $users, "inf" => $inf]);
    }

    public function create()
    {
        return view('admins.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required|max:255',
            'username'  => 'required|max:255',
            'email'     => 'required|email|max:255',
            'password'  => 'required|min:6|max:32',
        ]);

        if($validator->fails()){
            return redirect('/admin/administrator/create')->withErrors($validator);
        }

        $text = User::where('username', $request->input('username'))->first();
        if($text) {
            return redirect('/admin/administrator/create')->withErrors("This user has already been registered.");
        }

        $_image = new Image;
        if(($img_res = (new Controller)->UploadImage($request, "image"))) {
            $_image->name       = $img_res;
        }
        $_image->save();

        $user = new User;
        $user->name             = $request->input('name');
        $user->username         = $request->input('username');
        $user->email            = $request->input('email');
        $user->password         = bcrypt($request->input('password'));
        $user->image_id         = $_image->id;
        $user->save();

        return redirect('/admin/administrator/'.$user->id);
    }

    public function show($id)
    {
        $user = User::with('image')->findOrFail($id);
        return view('admins.show', compact("user"));
    }

    public function edit($id)
    {
        $user = User::with('image')->findOrFail($id);
        return view('admins.edit', compact("user"));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|max:255',
        ]);

        if($validator->fails()) {
            return redirect('/admin/administrator/'.$id.'/edit')->withErrors($validator);
        }

        $user = User::findOrFail($id);
        $user->name             = $request->input('name');
        $user->username         = $request->input('username');
        $user->email            = $request->input('email');
        $user->password         = bcrypt($request->input('password'));
        $user->save();

        $_image = Image::findOrFail($user->image_id);
        if(($img_res = (new Controller)->UploadImage($request, "image"))) {
            $_image->name               = $img_res;
        }
        $_image->save();

        return redirect('/admin/administrator/'.$user->id);
    }

    public function destroy($id)
    {
        if(!User::destroy($id)) {
            return redirect("/admin/administrator");
        }
        return $this->index("User deleted!");
    }
}
