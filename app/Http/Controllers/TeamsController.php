<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Content;
use App\Model\Team;
use App\Model\Other\Image;
use App\Model\Other\Link;
use \Session;
use \Validator;
use App\Mail\SenderAll;

class TeamsController extends Controller
{
    public function index()
    {
        $teams = Team::with('links', 'image')->paginate(10);
        return view('teams.index', ['teams' => $teams]);
    }

    public function create()
    {
        $contents = Content::all();
        return view('teams.create', ["contents" => $contents]);
    }

    public function show($id)
    {
        $team = Team::with('links', 'image')->findOrFail($id);
        return view('teams.show', compact("team"));
    }

    public function edit($id)
    {
        $contents = Content::all();
        $team = Team::with('links','image')->findOrFail($id);
        return view('teams.edit', ["team" => $team, "contents" => $contents]);
    }

    public function store(Request $request)
    {
        $team = new Team;
        $validator = Validator::make($request->all(), $team->rules);

        if ($validator->fails()) {
            return redirect('/admin/team/create')->withErrors($validator);
        }

        $_image = new Image;
        if(($img_res = (new Controller)->UploadImage($request, "image"))) {
            $_image->name       = $img_res;
        }
        $_image->save();

        $links = new Link;
        $links->facebook        = $request->input('fb');
        $links->twitter         = $request->input('tw');
        $links->google_plus     = $request->input('g');
        $links->linkedin        = $request->input('in');
        $links->vkontakte       = $request->input('vk');
        $links->save();

        $team->name             = $request->input('name');
        $team->position         = $request->input('position');
        $team->content_id       = $request->input('content_id');
        $team->link_id          = $links->id;
        $team->image_id         = $_image->id;
        $team->save();

        $_SENDERALL = new SenderAll();
        $_SENDERALL->send(
            "New information on the site FamBox.",
            "The Team had added a new team ($team->title)..",
            $team->updated_at,
            "Go to the portfolios Team.",
            "/team");

        Session::flash('flash_message', 'Pages Successfully created!');
        return redirect('/admin/team/');
    }

    public function update(Request $request, $id)
    {
        $team = Team::findOrFail($id);
        $validator = Validator::make($request->all(), $team->rules);
        
        if ($validator->fails()) {
            return redirect("/admin/team/$id/edit")->withErrors($validator);
        }
        
        $_image = new Image;
        if(($img_res = (new Controller)->UploadImage($request, "image"))) {
            $_image->name       = $img_res;
        }
        $_image->save();

        $team->name             = $request->input('name');
        $team->position         = $request->input('position');
        $team->content_id       = $request->input('content_id');
        $team->image_id         = $_image->id;

        
        if(is_null(($links = Link::find($team->link_id))))
            $links = new Link;
        $links->facebook        = $request->input('fb');
        $links->twitter         = $request->input('tw');
        $links->google_plus     = $request->input('g');
        $links->linkedin        = $request->input('in');
        $links->vkontakte       = $request->input('vk');
        $links->save();

        $team->link_id         = $links->id;
        $team->save();
        
        Session::flash('flash_message', 'Team Successfully updated!');
        return redirect('/admin/team/');
    }

    public function destroy($id)
    {
        Team::destroy($id);
        Session::flash('flash_message', 'Team deleted!');
        return redirect('/admin/team/');
    }
}
