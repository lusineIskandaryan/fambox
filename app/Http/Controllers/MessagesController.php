<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Model\Message;
use App\Model\Other\Answer;

class MessagesController extends Controller
{
    public function index()
    {
        return redirect("/admin/msg/notcomment");
    }

    public function create()
    {
        return redirect("/admin/msg/notcomment");
    }

    public function show($id)
    {
        if (strstr($id, "notverified"))
            $msgComment = ["msg" => Message::whereNull('answer_id')->get(), 'header_text' => 'The list of unanswered messages.'];

        else if (strstr($id, "verified"))
            $msgComment = ["msg" => Message::with('answers')->whereNotNull('answer_id')->get(), 'header_text' => 'List of viewed messages.'];
        else return redirect("/admin/msg/notverified");

        return view('messager.msg', $msgComment);
    }

    public function edit($id)
    {
        return redirect("/admin/msg/notcomment");
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'subject' => 'required|max:255',
            'message' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response($errors->toJson(), 404);
        }

        $msg = new Message;
        $msg->name          = $request->input('name');
        $msg->email         = $request->input('email');
        $msg->subject       = $request->input('subject');
        $msg->content       = $request->input('message');
        $msg->save();

        return response(['Message' => 'OK'], 201);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'answer' => 'required'
        ]);

        if (!$validator->fails()) {
            $user_id    = Auth::user()->id;
            $name       = Auth::user()->name;
            $_answer     = $request->input("answer");

            $answer = new Answer;
            $answer->answer    = $answer;
            $answer->user_id   = $user_id;
            $answer->save();

            $msg = Message::find($id);
            $msg->answer_id = $answer->id;

            # EMAIL
            $settings = [
                'admin' => $name, 'admin_comment' => $_answer, 'admin_created_at' => $answer->created_at,
                'user' => $msg->name, 'user_comment' => $msg->msg, 'user_created_at' => $msg->created_at
            ];
            Mail::to($msg->email)->send(new MailClass($settings));

            return $msg->save() ? "OK" : "ERROR";
        }
        return "ERROR";
    }

    public function destroy($id)
    {
        //
    }
}
