<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Model\Content;
use App\Model\Portfolio;
use App\Model\Other\Image;
use \Session;
use \Validator;
use App\Mail\SenderAll;

class PortfoliosController extends Controller
{
    public function index()
    {
        $portfolios = Portfolio::paginate(10);
        return view('portfolios.index', ['portfolios' => $portfolios]);
    }

    public function create()
    {
        $contents = Content::all();
        return view('portfolios.create', ["contents" => $contents]);
    }

    public function show($id)
    {
        $portfolio = Portfolio::with('image')->findOrFail($id);
        return view('portfolios.show', compact("portfolio"));
    }

    public function edit($id)
    {
        $contents = Content::all();
        $portfolio = Portfolio::with('image')->findOrFail($id);
        return view('portfolios.edit', ["portfolio" => $portfolio, "contents" => $contents]);
    }

    public function store(Request $request)
    {
        $portfolio = new Portfolio;
        $validator = Validator::make($request->all(), $portfolio->rules);
        
        if ($validator->fails()) {
            return redirect('/admin/portfolio/create')->withErrors($validator);
        }

        $_image = new Image;
        if(($img_res = (new Controller)->UploadImage($request, "image"))) {
            $_image->name               = $img_res;
        }
        $_image->save();

        $portfolio->title       = $request->input('title');
        $portfolio->content     = $request->input('content');
        $portfolio->link        = $request->input('link');
        $portfolio->status      = $request->input('status');
        $portfolio->content_id  = $request->input('content_id');
        $portfolio->image_id    = $_image->id;
        $portfolio->save();

        $_SENDERALL = new SenderAll();
        $_SENDERALL->send(
            "New information on the site FamBox.",
            "The page had added a new portfolio ($portfolio->title)..",
            $portfolio->updated_at,
            "Go to the portfolios page.",
            "/portfolio");

        Session::flash('flash_message', 'Portfolio Successfully created!');
        return redirect('/admin/portfolio/');
    }

    public function update(Request $request, $id)
    {
        $portfolio = Portfolio::findOrFail($id);
        $validator = Validator::make($request->all(), $portfolio->rules);

        if ($validator->fails()) {
            return redirect("/admin/portfolio/$id/edit")->withErrors($validator);
        }
        
        $_image = new Image;
        if(($img_res = (new Controller)->UploadImage($request, "image"))) {
            $_image->name               = $img_res;
        }
        $_image->save();

        $portfolio->title       = $request->input('title');
        $portfolio->content     = $request->input('content');
        $portfolio->link        = $request->input('link');
        $portfolio->status      = $request->input('status');
        $portfolio->content_id  = $request->input('content_id');
        $portfolio->image_id    = $_image->id;
        $portfolio->save();
        
        Session::flash('flash_message', 'Portfolio Successfully updated!');
        return redirect('/admin/portfolio/');
    }

    public function destroy($id)
    {
        Portfolio::destroy($id);
        Session::flash('flash_message', 'Portfolio deleted!');
        return redirect('/admin/portfolio/');
    }
}
