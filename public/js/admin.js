function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#img-avatar').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("input[name=avatar]").change(function() {
    readURL(this);
});

$("#clear-btn").click(function() {
    $("input[name=avatar]").val(null);
    $('#img-avatar').attr('src', '/images/profile/user.png');
});

var divs = document.getElementsByClassName("text");
for (var i = 0; i < divs.length; i++) {
    var str = divs[i].innerHTML;
    if (str.length >= 100) {
        divs[i].innerHTML = str.substr(str.length - 65) + "...";
    }
}

var _get = JSON.parse(localStorage.getItem('_menu'));
if (_get) {
    var _name = _get['name'];
    $('#' + _name).attr('aria-expanded', 'true');
    var ul = $('#' + _name).parent().children('ul');
    ul.attr('class', 'list-unstyled collapse show');

    console.log($(ul.children('li')[0]).children('a').attr('class', 'activeka'));
}

$("#storage-dropdown li a").click(function() {
    if ($(this).parent().parent().attr('id') == "storage-dropdown") {
        var _ulId = $(this).attr('id');
        localStorage.setItem('_menu', JSON.stringify({ 'name': _ulId }));
    }
});