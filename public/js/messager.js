function showCommen(e) {
    $(e.parentElement.parentElement).children("form").toggle();
    $(e.parentElement.parentElement).children("#inf").toggle();
};

$("form").submit(function(e) {
    e.preventDefault();
    setTimeout(function() {
        // Текст комментрий
        var comment_text = $(e.target).children().children("textarea").val();
        if (comment_text.length !== 0) {
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": e.currentTarget.action,
                "method": "PUT",
                "headers": { "content-type": "application/json" },
                "processData": false,
                "data": JSON.stringify({ answer: comment_text })
            }

            $.ajax(settings).done(function(response) {
                if (response == "OK") {
                    $('#msgcount').text(($('#msgcount').text() -= 1));
                    $(e.target.parentElement.parentElement).toggle();
                }
            });
        } else {
            alert("The 'comment' field is empty.");
        }
    }, 500);
});