<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

# API
Route::post('/test',             'AdminController@test');
Route::get('/menu',             'MenuController@api');
Route::get('/{item}/{id?}',           'ContentsController@api')->where(["item", "id"], ["[A-Za-z_]+", "[0-9]+"]);

Route::post('/email',           'MessagesController@store');
Route::post('/subscribe',       'SubscribesController@api');
