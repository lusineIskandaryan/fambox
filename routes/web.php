<?php

# аутентификация
Auth::routes();

# HOME
Route::get('/admin', function () {
    return redirect("/admin/msg");
});

# MENU
Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function() {
    Route::resource('/msg',             'MessagesController');
    Route::resource('/administrator',   'AdminController');

    Route::resource('/portfolio',       'PortfoliosController');
    Route::resource('/service',         'ServicesController');
    Route::resource('/page',            'PagesController');
    Route::resource('/team',            'TeamsController');
    Route::resource('/testimonial',     'TestimonialsController');
    Route::resource('/content',        'ContentsController');
    Route::resource('/menue',           'MenuController');
    Route::group(['prefix' => 'subscribe'], function () {
        Route::get('/',                 'SubscribesController@index');
        Route::put('/{id}',             'SubscribesController@update')->where('id', '[0-9]');
    });

    Route::resource('/template',         'MailTemplate');
    Route::get('/template/{id}/preview', 'MailTemplate@preview');
});

Route::get('/subscribe/{id}/{status}', 'SubscribesController@status')->where(["id", "status"], ["[a-f0-9]+", "^[0-1]"]);

# php artisan serve --host=192.168.1.202 --port=8000






























/**************************Angular Routes********************************************/
Route::get('/', function () {
    return view('index');
});

Route::get('/main/{any?}', function () {
    return view('index');
});

Route::get('/home/{any?}', function () {
    return view('index');
});

Route::get('/portfolio/{any?}', function () {
    return view('index');
});

Route::get('/team/{any?}', function () {
    return view('index');
});

Route::get('/member/{any?}', function () {
    return view('index');
});

Route::get('/about/{any?}', function () {
    return view('index');
});

Route::get('/services/{any?}', function () {
    return view('index');
});

Route::get('/testimonials/{any?}', function () {
    return view('index');
});

Route::get('/contact/{any?}', function () {
    return view('index');
});